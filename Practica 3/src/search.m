function index_start = search(vector,bits)
    string_look = join(string(vector'));
    last_i = 1;
    tic
    index_start = 0;
    for i=numel(vector):numel(vector):numel(bits)
        current_piece = join(string(bits(last_i:i)'));
        %disp(current_piece)
        %fprintf("%i:%i\n\r",last_i,i);
        if(current_piece == string_look)
            fprintf("encontrado en = %i:%i\n\r",last_i,i);
            index_start = last_i;
        end
        last_i = i+1;
    end
    toc
end