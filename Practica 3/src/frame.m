classdef frame
    properties
        PREAMBLE
        SFD
        DSA
        HEADER
        PAYLOAD
        bits2tx
        preamble_size = 56;
        SFD_size = 8;
        DSA_size = 120;
        HEADER_size = 20;
    end
    methods
        function obj = frame()
            
        end
        function [start_i end_i] = preamble_detect(obj, bits)
            vector = [1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0];
            string_look = join(string(vector'));
            last_i = 1;
            start_i = 0;
            tic
            for i=numel(vector):numel(vector):numel(bits)
                current_piece = join(string(bits(last_i:i)'));
                if(current_piece == string_look)
                    fprintf("preambulo encontrado en = %i:%i\n\r",last_i,i);
                    start_i = last_i;
                    end_i = i;
                end
                last_i = i+1;
            end
            toc
        end
        function obj = setFrameSection(obj, frame_section, data)
            if(frame_section == "PREAMBLE")
                obj.PREAMBLE = data';    
            elseif(frame_section == "SFD")
                obj.SFD = data';     
            elseif(frame_section == "DSA")
                obj.DSA = de2bi(uint8(data),8,'left-msb');
                obj.DSA = reshape(obj.DSA',1,numel(obj.DSA));
                obj.DSA = obj.DSA';
            elseif(frame_section == "HEADER")
                %{data expected to be in binary form %}
                obj.HEADER = [data(1:10) data(11:20)]';
            elseif(frame_section == "PAYLOAD")
                obj.PAYLOAD=data;
            else 
                % do nothing
            end
            obj.bits2tx = getBits2Transmit(obj);
        end
        function obj = setReceivedFrameSection(obj, frame_section, data)
            if(frame_section == "PREAMBLE")
                obj.PREAMBLE = data';    
            elseif(frame_section == "SFD")
                obj.SFD = data';     
            elseif(frame_section == "DSA")
                obj.DSA = data';
            elseif(frame_section == "HEADER")
                %{data expected to be in binary form %}
                obj.HEADER = [data(1:10) data(11:20)]';
            elseif(frame_section == "PAYLOAD")
                obj.PAYLOAD=data;
            else 
                % do nothing
            end
            obj.bits2tx = getBits2Transmit(obj);
        end
        function str = interpretDSA(obj, DSA)
            last_i = 1;
            str = '';
            for i= 8:8:numel(obj.DSA')
                fprintf("%i:%i\n", last_i,i);
                str = strcat(str, char(bi2de(obj.DSA(last_i:i)',2,'left-msb')));
                last_i = i+1;
            end
        end
        function pload = interpretPAYLOAD(obj, PAYLOAD)
            last_i = 1;
            pload = [];
            row = 0;
            for i= 8:8:numel(PAYLOAD)
                %fprintf("%i:%i\n", last_i,i);
                if(row > 511)
                    pload = [pload,bi2de(obj.PAYLOAD(last_i:i)',2,'left-msb')];
                else
                    pload = [pload;bi2de(obj.PAYLOAD(last_i:i)',2,'left-msb')];
                    row = -1;
                end
                row = row;
                last_i = i+1;
                   
            end
        end
        function data = interpretHeader(obj, HEADER, element)
            if(element == 0)
                data = bi2de(HEADER(1:10)','left-msb');
            else
                data = bi2de(HEADER(10:20)','left-msb');
            end
        end
        function frame_ss = getFrameSectionSize(obj, frame_section)
            if(frame_section == "PREAMBLE")
                frame_ss = numel(obj.PREAMBLE);    
            elseif(frame_section == "SFD")
                frame_ss = numel(obj.SFD);        
            elseif(frame_section == "DSA")
                frame_ss = numel(obj.DSA);    
            elseif(frame_section == "HEADER")
                frame_ss = numel(obj.HEADER);    
            elseif(frame_section == "PAYLOAD")
                frame_ss = numel(obj.PAYLOAD);    
            else 
                % do nothing
            end
        end
        function frame_bits = getBits2Transmit(obj)
            frame_bits = [obj.PREAMBLE; obj.SFD; obj.DSA; obj.HEADER; obj.PAYLOAD];
        end
        function ber = error_estimate(obj,bits_transmitted,bits_received)
            global bits_size
        
            ber = biterr(bits_transmitted,bits_received)/numel(bits_transmitted);
            ber = ber*100;
        end
    end
end
