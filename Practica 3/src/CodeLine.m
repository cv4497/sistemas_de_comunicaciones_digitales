classdef CodeLine
    properties
        Fs
        rb
        bits
        pulse
        mp
        xPulse 
        t
        type
    end
    methods 
        function obj = CodeLine(type_cl, pulse, mp, Fs)
            frame_obj = evalin('base', 'frame_obj');
            obj.Fs = Fs;
            obj.rb = Fs/mp;
            obj.bits = frame_obj.bits2tx;
            obj.pulse = pulse;
            obj.mp = mp;
            obj.type = type_cl;
            if(type_cl == "PNRZ")
                [obj.xPulse obj.t]= polarNRZ(obj, frame_obj.bits2tx, obj.pulse, obj.mp, obj.Fs);
            end
        end
        function transmit(obj)
            [X, Fs] = audioread('transmission.wav');
            soundsc(zeros(1, Fs/2), Fs);
            soundsc(X, Fs);
        end
        function calcTime(obj, Rb) 
            t_total = (numel(obj.bits)/Rb)/60;
            fprintf("numero de bits = %.2f bits = %.2f Mbytes\n",numel(obj.bits),numel(obj.bits)/8/1000000);
            fprintf("bitrate (rb) = %.2f bps\n", Rb);
            fprintf("tiempo de transmision = %.4f seg\n\r", t_total)
        end
        function codeLinePlot(obj,n_pulses)
            figure;
            if(n_pulses == '*')
                plot(obj.t,obj.xPulse,'b');
            else
                plot(obj.t(1:obj.mp*n_pulses),obj.xPulse(1:obj.mp*n_pulses),'b');
            end
            title(strcat("CODIGO DE LINEA ",obj.type));
            xlabel('tiempo (segundos)');
            ylabel('amplitud (volts)');
            legend({'Codigo de Linea PNRZ'},'Location','northeast','Orientation','vertical')
        end
        function writeTransmissionAudio(obj)
            audiowrite('transmission.wav', obj.xPulse, obj.Fs);
        end
        function eyeDiagram(obj)
            ed1 = comm.EyeDiagram('SampleRate',obj.Fs*obj.mp,'SamplesPerSymbol',obj.mp);
            assignin('base', 'ed1', ed1);
            delay= round(numel(obj.pulse)/2);
            ed1(obj.xPulse(delay+1:end-delay)');
        end
        function [xPNRZ t] = polarNRZ(obj, bits, pulse, mp, Fs)
            %pulse_obj = evalin('base', 'pulse_obj');
            bits = int8(bits);
            rs = Fs/mp;
            for i = 1:numel(bits)
             if(bits(i) == 0)
                bits(i) = -1;
             end
            end
            s = zeros(1,numel(bits)*mp);
            s(1:mp:end) = bits;
            xPNRZ = conv(pulse,s);
            xPNRZ = xPNRZ/max(abs(xPNRZ));
            
            assignin('base','xPNRZ', xPNRZ);
            TotalTime = length(xPNRZ)./Fs;                    % duracion del audio
            dt = (TotalTime/(length(xPNRZ)));                 % paso de tiempo
            tf = (TotalTime-(TotalTime/length(xPNRZ)));       % tiempo final
            t = 0:dt:tf;                                  % definicion del v. tiempo
            figure; 
            plot(t, xPNRZ);
        end
    end
end