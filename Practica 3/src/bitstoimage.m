function new_mat = bits_to_image(receivedBits)
    global header;
    global size_img;
    %receivedBits =  uint8(receivedBits);

    bits_to_matrix = vec2mat(receivedBits,8);
    lena_matrix = bi2de(bits_to_matrix,'left-msb');
    new_mat = (vec2mat(lena_matrix,512))';
end
