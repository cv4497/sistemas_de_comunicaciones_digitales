function PNRZ = polarNRZ(bits, pulse, mp, Fs)
    bits = int8(bits);
    rs = Fs/mp;

    for i = 1:numel(bits)
     if(bits(i) == 0)
        bits(i) = -1;
    end
    s = zeros(1,numel(bits)*mp);
    s(1:mp:end) = bits;
    PNRZ = conv(pulse,s);
end