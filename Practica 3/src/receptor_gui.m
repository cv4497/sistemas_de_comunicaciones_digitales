function varargout = receptor_gui(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @receptor_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @receptor_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT

% --- Executes just before receptor_gui is made visible.
function receptor_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to receptor_gui (see VARARGIN)

% Choose default command line output for receptor_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
movegui(hObject, 'center');

% UIWAIT makes receptor_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global transmit_flag plot_flag ed_flag write_audio frame_obj;
frame_obj = frame;
frame_obj = setFrameSection(frame_obj,"PREAMBLE", [1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0]);
frame_obj = setFrameSection(frame_obj,"SFD", [1 0 1 0 1 0 1 1]);
frame_obj = setFrameSection(frame_obj,"DSA", 'practica3:cesar');
frame_obj = setFrameSection(frame_obj,"HEADER", [MyImage("lena512.mat").binaryWidth(), MyImage("lena512.mat").binaryHeight()]);
frame_obj = setFrameSection(frame_obj,"PAYLOAD", MyImage("lena512.mat").bits);
assignin('base', 'frame_obj', frame_obj);
transmit_flag = 0;
plot_flag = 0;
ed_flag = 0;
write_audio = 0;
txt_receiver_filename = evalin('base', 'txt_receiver_filename');
set(handles.axes1,'Visible', 'on');
set(handles.slider3, 'Value', 0.5);
if isfile(txt_receiver_filename)
    set(handles.pb_exec, 'Enable', 'on');
    set(handles.cb_synch, 'Enable', 'on');
    set(handles.cb_const, 'Enable', 'on');
    set(handles.cb_plt_ed, 'Enable', 'on');
    set(handles.cb_mf, 'Enable', 'on');
    set(handles.cb_welch, 'Enable', 'on');
else
    set(handles.pb_exec, 'Enable', 'off');
    set(handles.cb_synch, 'Enable', 'off');
    set(handles.cb_const, 'Enable', 'off');
    set(handles.cb_plt_ed, 'Enable', 'off');
    set(handles.cb_mf, 'Enable', 'off');
    set(handles.cb_welch, 'Enable', 'off');
end    

% --- Outputs from this function are returned to the command line.
function varargout = receptor_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
set(handles.txt_busy,'visible','off');
set(handles.txt_ready,'visible','off');

% --- Executes on button press in pb_exec.
function pb_exec_Callback(hObject, eventdata, handles)
% hObject    handle to pb_exec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global transmit_flag plot_flag ed_flag write_audio;
received_filename = evalin('base', 'txt_receiver_filename');
set(handles.txt_busy,'visible','on');
set(handles.txt_ready,'visible','off');

receiver(get(handles.cb_synch,'Value'), received_filename);

received_bits = evalin('base', 'received_bits');
decode_received_bits = evalin('base', 'decode_received_bits');
frame_obj = evalin('base', 'frame_obj');
WIDTH = evalin('base', 'WIDTH');
HEIGHT = evalin('base', 'HEIGHT');
DSA_MSG = evalin('base', 'DSA_MSG');
set(handles.txt_busy,'visible','off');
set(handles.txt_ready,'visible','on');

mf_xPNRZ = evalin('base', 'mf_xPNRZ');
%{
get(handles.rb_plt_sig,'Value')
 get(handles.rb_plt_img, 'Value')
  get(handles.cb_plt_ed, 'Value')
   get(handles.cb_const, 'Value')
   get(handles.cb_welch, 'Value')
%}

if(get(handles.rb_plt_img, 'Value') == 1)
    img_received = uint8(evalin('base', 'img_received'));
    set(handles.axes1,'ButtonDownFcn',@axes1_ButtonDown);
    ax = get(handles.axes1,'ButtonDownFcn')
    %imshow(img_received,'Parent',handles.axes1);
    imagesc(uint8(img_received),'Parent',handles.axes1,'HitTest', 'off');
    set(handles.axes1,'colormap',gray);
    set(handles.axes1,'ButtonDownFcn',ax);
    set(handles.slider3, 'Enable', 'off')
end

if(get(handles.rb_plt_sig,'Value') == 1)
     t = evalin('base', 't_frame');
     x_frame = evalin('base', 'x_frame');
     cl_obj = evalin('base', 'cl_obj');
     set(handles.slider3, 'Enable', 'on');
     %{ grafica de la senal muestreada %}
     axes(handles.axes1)
     set(handles.axes1,'ButtonDownFcn',@axes1_signal);
     ax = get(handles.axes1,'ButtonDownFcn')
     plot(t(1:get(handles.slider3,'Value')*1000*cl_obj.mp),x_frame(1:get(handles.slider3,'Value')*1000*cl_obj.mp),'r'); 
     title('Grafica del frame recibido');
     xlabel('tiempo'); ylabel('amplitud');
     xlim([t(1) t(get(handles.slider3,'Value')*1000*cl_obj.mp)]);
     set(handles.axes1,'ButtonDownFcn',ax);
end

if(get(handles.cb_const,'Value') == 1)
    mf_xPNRZ = evalin('base', 'mf_xPNRZ');
    M = 2;
    refC = qammod(0:M-1,M);
    constdiag2 = comm.ConstellationDiagram('ReferenceConstellation',refC, ...
    'XLimits',[-2 2],'YLimits',[-2 2]);
    constdiag2(mf_xPNRZ');
    assignin('base','constdiag2',constdiag2);
end

if(get(handles.cb_welch,'Value') == 1)
    mf_xPNRZ = evalin('base', 'mf_xPNRZ');
    pulse_obj =  evalin('base', 'pulse_obj');
    figure;
    pwelch(mf_xPNRZ, [], [], [], pulse_obj.Fs,'power');
    title('Espectro de potencia del frame');

end

if(get(handles.cb_plt_ed,'Value') == 1)
    mf_xPNRZ = evalin('base', 'mf_xPNRZ');
    pulse_obj =  evalin('base', 'pulse_obj');
    ed2 = comm.EyeDiagram('SampleRate',pulse_obj.Fs*pulse_obj.mp,'SamplesPerSymbol',pulse_obj.mp);
    delay= round(numel(pulse_obj.pbase)/2);
    ed2(mf_xPNRZ(delay+1:end-delay)'); % xpnrz es el tren de pulsos Tx polar NRZ
    assignin('base', 'ed2', ed2);
end
frame_received = evalin('base', 'frame_received');
set(handles.txt_br,'String',strcat(string(received_bits/1000000),' MB'));
set(handles.txt_height,'String',HEIGHT);
set(handles.txt_width,'String',WIDTH);
set(handles.txt_dsa,'String',DSA_MSG);
err_preamble =frame_obj.error_estimate(frame_obj.PREAMBLE, frame_received.PREAMBLE);
err_sfd = frame_obj.error_estimate(frame_obj.SFD, frame_received.SFD);
err_dsa = frame_obj.error_estimate(frame_obj.DSA, frame_received.DSA);
err_header = frame_obj.error_estimate(frame_obj.HEADER, frame_received.HEADER);
err_payload = frame_obj.error_estimate(frame_obj.PAYLOAD(1:numel(frame_received.PAYLOAD)), frame_received.PAYLOAD);
fprintf("err_preamble = %.1f\n",err_preamble);
fprintf("err_sfd = %.1f\n",err_sfd);
fprintf("err_dsa = %.1f\n",err_dsa);
fprintf("err_header = %.1f\n",err_header);
fprintf("err_payload = %.1f\n",err_payload);

set(handles.txt_berr, 'String',err);
set(handles.axes1,'xtick',[])
set(handles.axes1,'ytick',[])
position = get(handles.axes1,'position');
disp(position(3));
set(handles.axes1,'Position',[position(1) position(2) position(3) position(4)]);

% --- Executes on button press in pb_ret.
function pb_ret_Callback(hObject, eventdata, handles)
% hObject    handle to pb_ret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(receptor_gui);
set(gui,'visible','on');

% --- Executes on button press in cb_synch.
function cb_synch_Callback(hObject, eventdata, handles)
% hObject    handle to cb_synch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global plot_flag;
plot_flag = get(hObject,'Value');


% --- Executes on button press in cb_const.
function cb_const_Callback(hObject, eventdata, handles)
% hObject    handle to cb_const (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_const
global write_audio;
write_audio = get(hObject,'Value');


% --- Executes on button press in cb_plt_ed.
function cb_plt_ed_Callback(hObject, eventdata, handles)
% hObject    handle to cb_plt_ed (see GCBO)
% eventdata  reserved - to be dget(hObject,'Value');efined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ed_flag;
ed_flag = get(hObject,'Value');


% --- Executes on button press in cb_mf.
function cb_mf_Callback(hObject, eventdata, handles)
% hObject    handle to cb_mf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global transmit_flag;
transmit_flag = get(hObject,'Value');


% --- Executes on button press in cb_welch.
function cb_welch_Callback(hObject, eventdata, handles)
% hObject    handle to cb_welch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_welch

function axes1_ButtonDown(hObject, eventdata, handles)
try
    img_received = uint8(evalin('base', 'img_received'));
    
catch
    warning('Image received is not set yet.');
end
if exist('img_received','var') == 1
    disp("axes button down");
    figure('Name','Received Image');
    img_received = uint8(evalin('base', 'img_received'));
    imshow(img_received);
else
    disp("no image");
end

function axes1_signal(hObject, eventdata, handles)
    t = evalin('base', 't_frame');
    x_frame = evalin('base', 'x_frame');
    figure;
    plot(t,x_frame,'r'); 
    title('Grafica del frame recibido');
    xlabel('tiempo'); ylabel('amplitud');

function slider3_CreateFcn(hObject, eventdata, handles)

function slider3_Callback(hObject, eventdata, handles)
    try 
        x_frame = evalin('base', 'x_frame');
        t = evalin('base', 't_frame');
        cl_obj = evalin('base', 'cl_obj');
    catch
    end

    if exist('x_frame', 'var') == 1
        cl_obj = evalin('base', 'cl_obj');
        xlim([t(1) t(get(handles.slider3,'Value')*1000*cl_obj.mp)]);
    end

function rb_img_received_Callback(hObject, eventdata, handles)
set(handles.slider3, 'visible', 'off');

function rb_sig_received_Callback(hObject, eventdata, handles)
set(handles.slider3, 'visible', 'on');
