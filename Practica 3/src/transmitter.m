%{  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{
     Autor: Cesar Villarreal Hernandez
     Titulo: Practica III
     Descripcion: Modulo transmisor
     Asignatura: Sistemas de Comunicaciones Digitales
     Fecha: 29/noviembre/2020
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}

function transmitter(transmit_flag, plot_flag, ed_flag, write_audio)
    tic

    %{ ~~~~~~~~~~~~~~~~~~~~~~~~ 1. CONSTRUCCION DEL FRAME ~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    frame_obj = frame;
    frame_obj = setFrameSection(frame_obj,"PREAMBLE", [1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0]);
    frame_obj = setFrameSection(frame_obj,"SFD", [1 0 1 0 1 0 1 1]);
    frame_obj = setFrameSection(frame_obj,"DSA", 'practica3:cesar');
    frame_obj = setFrameSection(frame_obj,"HEADER", [MyImage("lena512.mat").binaryWidth(), MyImage("lena512.mat").binaryHeight()]);
    frame_obj = setFrameSection(frame_obj,"PAYLOAD", MyImage("lena512.mat").bits);
    assignin('base', 'frame_obj', frame_obj);

     %{ ~~~~~~~~~~~~~~~ 2. PULSO BASE ~~~~~~~~~~~~~~~~~~~~ %}
    pulse_obj = evalin('base', 'pulse_obj');

    %{ ~~~~~~~~~~~~~~~ 3. GENERACION DE CODIGO DE LINEA POLAR NRZ ~~~~~~~~~~~~~~~~~~~~ %}
    cl_obj = CodeLine("PNRZ", pulse_obj.getPulse(), pulse_obj.mp, pulse_obj.Fs);
    assignin('base', 'cl_obj', cl_obj);
    
    if(transmit_flag == 1)
        cl_obj.calcTime(pulse_obj.Rb); % calcular tiempo de transmision
        cl_obj.transmit();             % transmitir codigo de linea
    end

    if(plot_flag == 1)
        cl_obj.codeLinePlot(300);      
    end

    if(write_audio == 1)
        cl_obj.writeTransmissionAudio();
    end

    if(ed_flag == 1)
        cl_obj.eyeDiagram();           % mostrar diagrama de ojo
    end

    toc
end