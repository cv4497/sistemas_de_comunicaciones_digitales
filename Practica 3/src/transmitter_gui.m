function varargout = transmitter_gui(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @transmitter_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @transmitter_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT

% --- Executes just before transmitter_gui is made visible.
function transmitter_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to transmitter_gui (see VARARGIN)

% Choose default command line output for transmitter_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
movegui(hObject, 'center');

% UIWAIT makes transmitter_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
transmit_flag = 0;
plot_flag = 0;
ed_flag = 0;
write_audio = 0;

assignin('base', 'transmit_flag', transmit_flag);
assignin('base', 'plot_flag', plot_flag);
assignin('base', 'ed_flag', ed_flag);
assignin('base', 'write_audio', write_audio);

set(handles.txt_ex_time,'visible','off');
set(handles.text6,'visible','off');
set(handles.txt_seg,'visible','off');


% --- Outputs from this function are returned to the command line.
function varargout = transmitter_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
if isfile('transmission.wav')
    set(handles.cb_tx_cl, 'Enable', 'on')
else
    set(handles.cb_tx_cl, 'Enable', 'off')
end 
set(handles.txt_busy,'visible','off');
set(handles.txt_ready,'visible','off');
set(handles.txt_transmission, 'visible', 'off');


% --- Executes on button press in pb_exec.
function pb_exec_Callback(hObject, eventdata, handles)
% hObject    handle to pb_exec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
transmit_flag = evalin('base', 'transmit_flag');
plot_flag = evalin('base', 'plot_flag');
ed_flag = evalin('base', 'ed_flag');
write_audio = evalin('base', 'write_audio');
pulse_obj = evalin('base', 'pulse_obj');
if exist('ed1','var') == 1
release(ed1);
end
set(handles.txt_ready,'visible','off');
if(get(handles.cb_wr_audio,'Value'))
    set(handles.txt_transmission, 'visible', 'on');
    set(handles.txt_busy,'visible','off');
else
    set(handles.txt_transmission, 'visible', 'off');
    set(handles.txt_busy,'visible','on');
end
tic
transmitter(transmit_flag, plot_flag, ed_flag, write_audio);
set(handles.txt_ex_time,'visible','on');
set(handles.text6,'visible','on');
set(handles.txt_seg,'visible','on');
set(handles.txt_ex_time, 'String', string(toc));
set(handles.txt_busy,'visible','off');
set(handles.txt_ready,'visible','on');
set(handles.txt_transmission, 'visible', 'off');
if isfile('transmission.wav')
    set(handles.cb_tx_cl, 'Enable', 'on')
else
    set(handles.cb_tx_cl, 'Enable', 'off')
end 
set(handles.cb_tx_cl, 'Value', 0);
set(handles.cb_plt_ed, 'Value', 0);
set(handles.cb_wr_audio, 'Value', 0);
set(handles.cb_plt_cl, 'Value', 0);
transmit_flag = 0;
plot_flag = 0;
ed_flag = 0;
write_audio = 0;
assignin('base', 'transmit_flag', transmit_flag);
assignin('base', 'plot_flag', plot_flag);
assignin('base', 'ed_flag', ed_flag);
assignin('base', 'write_audio', write_audio);


% --- Executes on button press in pb_ret.
function pb_ret_Callback(hObject, eventdata, handles)
% hObject    handle to pb_ret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(transmitter_gui);
set(gui,'visible','on');

% --- Executes on button press in cb_plt_cl.
function cb_plt_cl_Callback(hObject, eventdata, handles)
% hObject    handle to cb_plt_cl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global plot_flag;
plot_flag = get(hObject,'Value');
assignin('base', 'plot_flag', plot_flag);


% --- Executes on button press in cb_wr_audio.
function cb_wr_audio_Callback(hObject, eventdata, handles)
% hObject    handle to cb_wr_audio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_wr_audio
write_audio = get(hObject,'Value');
assignin('base', 'write_audio', write_audio);

% --- Executes on button press in cb_plt_ed.
function cb_plt_ed_Callback(hObject, eventdata, handles)
% hObject    handle to cb_plt_ed (see GCBO)
% eventdata  reserved - to be dget(hObject,'Value');efined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ed_flag;
ed_flag = get(hObject,'Value');
assignin('base', 'ed_flag', ed_flag);


% --- Executes on button press in cb_tx_cl.
function cb_tx_cl_Callback(hObject, eventdata, handles)
% hObject    handle to cb_tx_cl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
transmit_flag = get(hObject,'Value');
assignin('base', 'transmit_flag', transmit_flag);