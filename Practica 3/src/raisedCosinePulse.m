classdef raisedCosinePulse
    properties
        Fs    % frecuencia de muestreo
        Ts    % periodo del pulso
        beta  % factor de roll-off
        Rb    % bit rate = Baud rate
        mp    % Muestras por pulso  
        Tp    % Symbol period
        B     % Ancho de banda consumido
        D     % Duracion de tiempo en terminos de tp
        type  % Forma del pulso: Square Root Rise Cosine
        E     % Energia
        pbase
    end
    methods
        function obj = raisedCosinePulse(Fs, beta, B, D)
            obj.Fs = Fs;
            obj.Ts = 1/Fs;
            obj.beta = beta;
            obj.B = B;
            obj.Rb = (2*B)/(1+beta);
            obj.mp = ceil(obj.Fs/obj.Rb);
            obj.Tp = 1/(obj.Rb); 
            obj.D = D;
            obj.E = obj.Tp;
            obj.type = 'srrc';
            [obj.pbase ~] = rcpulse(obj, obj.beta, obj.D, obj.Tp, obj.Ts, obj.type, obj.E); % Generacion del pulso
        end
        function pbase = getPulse(obj)
            pbase = obj.pbase;
        end
        function analyzePulse(obj)
            wvtool(obj.pbase)
        end
        function info(obj)
            fprintf(" Fs = %.1f kHz\n", obj.Fs/1000);
            fprintf(" Ts = %.1f us\n", obj.Ts*1000000);
            fprintf(" beta = %.2f\n", obj.beta);
            fprintf(" B = %.2f\n", obj.B);
            fprintf(" Rb = %.2f\n", obj.Rb);
            fprintf(" mp = %.2f\n", obj.mp);
            fprintf(" Tp = %.2f us\n", obj.Tp*1000000);
            fprintf(" D = %.2f seg\n", obj.D);
            fprintf(" E = %.2f uJ\n", obj.E*1000000);
        end
        function [p t] = rcpulse(obj,beta,D,Tp,Ts,type,energy)
            if nargin < 6
                energy = 1;
            end
            
            if nargin == 4
                type = 'rc';
            end
            
            %% para evitar problemas num�ricos con beta
            if beta==0
                beta=1e-12;
            end;
            
            % checar que Tp sea m�ltiplo entero de Ts
            if abs(round(Tp/Ts) - Tp/Ts) > 1e-6
                error('Error: Tp debe ser m�ltiplo de Ts');
            end
            
            % checar que D sea par
            if mod(D,2) == 1
                error('Error: D debe ser par');
            end
            
            %%% vector de tiempo (no causal)
            t = -D*Tp/2:Ts:D*Tp/2;
            
            if strcmp(type, 'srrc')
                % square-root raised cosine (si beta==0, regresa un sinc)
                % se calcula en tres partes: tiempo negativo, 0, y positivo
                t1 = -D*Tp/2:Ts:-Ts;
                t2 = Ts:Ts:D*Tp/2;
            
                x1 = pi*(1-beta)*t1/Tp;
                x2 = pi*(1+beta)*t1/Tp;
                x3 = 4*beta*t1/Tp;
                num = sin(x1)+x3.*cos(x2);
                den = sqrt(Tp).*(pi*t1./Tp).*(1-x3.^2);
                p1 = num./den;
                p1(find(abs(den)<1e-9))=(beta/sqrt(2*Tp))*...
                    ((1+2/pi)*sin(pi/(4*beta))+(1-2/pi)*cos(pi/(4*beta)));
            
                p2 = 1/sqrt(Tp)*(1-beta+4*beta/pi);
            
                x1 = pi*(1-beta)*t2/Tp;
                x2 = pi*(1+beta)*t2/Tp;
                x3 = 4*beta*t2/Tp;
                num = sin(x1)+x3.*cos(x2);
                den = sqrt(Tp).*(pi*t2./Tp).*(1-x3.^2);
                p3 = num./den;
                p3(find(abs(den)<1e-9))=(beta/sqrt(2*Tp))*...
                    ((1+2/pi)*sin(pi/(4*beta))+(1-2/pi)*cos(pi/(4*beta)));
            
                p = [p1 p2 p3];
            
            elseif strcmp(type, 'rc')
                % raised cosine (si beta==0, regresa un sinc)
                % s�lo hay que cuidar el caso donde el denominador se hace cero
                den = 1-(2*beta*t/Tp).^2;
                c = cos(pi*beta*t/Tp)./den;
                c(find(abs(den)<1e-9)) = pi/4;
                p = (1/Tp).*sinc(t/Tp).*c;
            end
            
            % hacer que el pulso sea causal
            t = t + D*Tp/2;
            
            % normalizacion
            en = trapz(t,p.*p);
            p = sqrt(energy/en)*p;
            end
    end
end