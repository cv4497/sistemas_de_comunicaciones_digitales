%{  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{
     Autor: Cesar Villarreal Hernandez
     Titulo: Practica III
     Descripcion: Modulo receptor
     Asignatura: Sistemas de Comunicaciones Digitales
     Fecha: 30/noviembre/2020
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}

function receiver_sync(received_filename, plot_flag, ed_flag)
    tic
    ftools_obj = frame;
    pulse_obj = evalin('base', 'pulse_obj');

    %{ ~~~~~~~~~~~~~~~~~~~ 7. GRAFICA DE LA SENAL RECIBIDA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    %[x_frame, t, Fs, dt, tf, TotalTime] = sample_audiofile(frame_file,20);
    [x_frame, Fs] = audioread(received_filename);
    x_frame = x_frame(:,1);

    %{ calculo del vector de tiempo %}
    TotalTime = length(x_frame)./Fs;                    % duracion del audio
    dt = (TotalTime/(length(x_frame)));                 % paso de tiempo
    tf = (TotalTime-(TotalTime/length(x_frame)));       % tiempo final
    t = 0:dt:tf;                                  % definicion del v. tiempo

    %{ ~~~~~~~~~~~~~~~~~~~ 8. QUITAR SILENCIO AL FRAME ~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    Fs=96e3; sec=5; % Time duration of the whole communication including the silence
    Rx_signal = x_frame;
    threshold = 0.5; % Detecting the channel energization
    start = find(abs(Rx_signal)> threshold,3,'first'); % Initial
    stop = find(abs(Rx_signal)> threshold,1,'last'); % End
    Rx_signal = Rx_signal (start:stop);
    %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 9. MATCHED FILTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    %{ Aplicar Matched filter}
    mf_xPNRZ = conv(pulse_obj.pbase,Rx_signal');
    %{ Normalización de la salida del match filter}
    mf_xPNRZ = mf_xPNRZ./(max(abs(mf_xPNRZ)));
    %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 10. SINCRONIZADOR ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    method = 'Early-Late (non-data-aided)';
    symbolSync = comm.SymbolSynchronizer('TimingErrorDetector', method, 'SamplesPerSymbol', pulse_obj.mp);
    rxSym = symbolSync(mf_xPNRZ');
    rxSym = double(rxSym');
    release(symbolSync);
    %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 11. DECODIFICACION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
    %bits2Tx = [preamble; SFD; DSA; header];
    decode_start = 1;
    decode_received_bits_sync = rxSym;
    decode_received_bits_sync = decode_received_bits_sync(:);
    decode_received_bits_sync(decode_received_bits_sync > 0) = 1;
    decode_received_bits_sync(decode_received_bits_sync < 0) = 0;

    preamble_bits = numel(frame_obj.PREAMBLE')
    SFD_bits = numel(frame_obj.SFD')
    DSA_bits = numel(frame_obj.DSA')
    HEADER_bits = numel(frame_obj.HEADER')
    payload_bits =  numel(frame_obj.PAYLOAD')

    preamble = decode_received_bits_sync(1:preamble_bits);
    SFD = decode_received_bits_sync(preamble_bits+decode_start:preamble_bits+SFD_bits+(decode_start-1));
    DSA = decode_received_bits_sync(preamble_bits+SFD_bits+1:preamble_bits+SFD_bits+DSA_bits);
    HEADER = decode_received_bits_sync(preamble_bits+SFD_bits+DSA_bits+1:preamble_bits+SFD_bits+DSA_bits+HEADER_bits);
    PAYLOAD_SYNC = decode_received_bits_sync(045329:numel(decode_received_bits_sync)); %(815329:numel(decode_received_bits_sync) %045329

    WIDTH = ftools_obj.interpretHeader(HEADER,0);
    HEIGHT = ftools_obj.interpretHeader(HEADER,1);
    disp(WIDTH)

    %{convertir bits a imagen}
    if(plot_img)
        img_received = bitstoimage(PAYLOAD_SYNC);
    else
        
    end

    if(plot_const)

    end

    if(plot_pwrspect)

    end

    if(plot_eyed)

    end

    %{mandar vectores de bits al workspace }
    assignin('base', 'decode_received_bits_sync', decode_received_bits_sync);
    assignin('base', 'preamble_sync', preamble);
    assignin('base', 'SFD_sync', SFD);
    assignin('base', 'DSA_sync', DSA);
    assignin('base', 'HEADER_sync', HEADER);
    assignin('base', 'PAYLOAD_SYNC', PAYLOAD_SYNC);
    assignin('base', 'img_received', img_received);

    %{la imagen es mostrada en la GUI del receptor}
    toc
end