function varargout = config(varargin)
%CONFIG MATLAB code file for config.fig
%      CONFIG, by itself, creates a new CONFIG or raises the existing
%      singleton*.
%
%      H = CONFIG returns the handle to a new CONFIG or the handle to
%      the existing singleton*.
%
%      CONFIG('Property','Value',...) creates a new CONFIG using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to config_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      CONFIG('CALLBACK') and CONFIG('CALLBACK',hObject,...) call the
%      local function named CALLBACK in CONFIG.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help config

% Last Modified by GUIDE v2.5 30-Nov-2020 15:03:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @config_OpeningFcn, ...
                   'gui_OutputFcn',  @config_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before config is made visible.
function config_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
global Fs beta BW D;
Fs = 96e3;
beta = 0.20;
BW = 7200;
D = 6;

txtbox_fs = findobj(0, 'tag', 'txtbox_fs');
txtbox_beta = findobj(0, 'tag', 'txtbox_beta');
txtbox_bw = findobj(0, 'tag', 'txtbox_bw');
txtbox_d = findobj(0, 'tag', 'txtbox_d');
set(txtbox_fs, 'String', Fs);
set(txtbox_beta , 'String', beta);
set(txtbox_bw, 'String', BW);
set(txtbox_d, 'String', D);

% Choose default command line output for config
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
movegui(hObject, 'center');

% UIWAIT makes config wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = config_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_gpb.
function button_gpb_Callback(hObject, eventdata, handles)
global Fs beta BW D;
% hObject    handle to button_gpb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pulse_obj = raisedCosinePulse(Fs, beta, BW, D)
txtbox_rb = findobj(0, 'tag', 'txtbox_rb');
txtbox_mp = findobj(0, 'tag', 'txtbox_mp');
txtbox_e = findobj(0, 'tag', 'txtbox_e');
button_apb = findobj(0, 'tag', 'button_apb');

set(txtbox_rb, 'String', pulse_obj.Rb);
set(txtbox_mp, 'String', pulse_obj.mp);
set(txtbox_e, 'String', pulse_obj.E);
set(button_apb,'Enable','on');
assignin('base', 'pulse_obj', pulse_obj);

% --- Executes on button press in button_rst.
function button_rst_Callback(hObject, eventdata, handles)
% hObject    handle to button_rst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete 'transmission.wav';
close all; clc;
gui;

% --- Executes on button press in button_ret.
function button_ret_Callback(hObject, eventdata, handles)
% hObject    handle to button_ret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(gui,'visible','on');
set(config,'visible','off');
txt_receiver_filename =  strcat(get(handles.txt_receiver_filename,'String'),'.wav');
assignin('base', 'txt_receiver_filename', txt_receiver_filename);


% --- Executes on button press in button_apb.
function button_apb_Callback(hObject, eventdata, handles)
% hObject    handle to button_apb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pulse_obj = evalin('base', 'pulse_obj');
pulse_obj.analyzePulse();


function txtbox_fs_Callback(hObject, eventdata, handles)
% hObject    handle to txtbox_fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtbox_fs as text
%        str2double(get(hObject,'String')) returns contents of txtbox_fs as a double
global Fs;
Fs = get(hObject,'Value');

% --- Executes during object creation, after setting all properties.
function txtbox_fs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtbox_fs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtbox_bw_Callback(hObject, eventdata, handles)
% hObject    handle to txtbox_bw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtbox_bw as text
%        str2double(get(hObject,'String')) returns contents of txtbox_bw as a double
global BW;
BW = get(hObject,'Value');


% --- Executes during object creation, after setting all properties.
function txtbox_bw_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtbox_bw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtbox_beta_Callback(hObject, eventdata, handles)
% hObject    handle to txtbox_beta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtbox_beta as text
%        str2double(get(hObject,'String')) returns contents of txtbox_beta as a double


% --- Executes during object creation, after setting all properties.
function txtbox_beta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtbox_beta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtbox_d_Callback(hObject, eventdata, handles)
% hObject    handle to txtbox_d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtbox_d as text
%        str2double(get(hObject,'String')) returns contents of txtbox_d as a double
global D;
D = get(hObject,'Value');


% --- Executes during object creation, after setting all properties.
function txtbox_d_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtbox_d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
