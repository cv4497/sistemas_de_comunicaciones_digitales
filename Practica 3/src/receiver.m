%{  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{
     Autor: Cesar Villarreal Hernandez
     Titulo: Practica III
     Descripcion: Modulo receptor
     Asignatura: Sistemas de Comunicaciones Digitales
     Fecha: 30/noviembre/2020
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}

function receiver(sync, received_filename)
     tic
     pulse_obj = evalin('base', 'pulse_obj');
     ftools_obj = frame;

     %{ ~~~~~~~~~~~~~~~~~~~ 7. GRAFICA DE LA SENAL RECIBIDA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
     %[x_frame, t, Fs, dt, tf, TotalTime] = sample_audiofile(frame_file,20);
     [x_frame, Fs] = audioread(received_filename);
     x_frame = x_frame(:,1);
     %{ ~~~~~~~~~~~~~~~~~~~ 8. QUITAR SILENCIO AL FRAME ~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
     Fs=96e3;  % Time duration of the whole communication including the silence
     Rx_signal = x_frame;
     threshold = 0.1; % Detecting the channel energization
     start = find(abs(Rx_signal)> threshold,3,'first'); % Initial
     stop = find(abs(Rx_signal)> threshold,1,'last'); % End
     Rx_signal = Rx_signal (start:stop);

     %{ calculo del vector de tiempo %}
     TotalTime = length(Rx_signal)./Fs;                    % duracion del audio
     dt = (TotalTime/(length(Rx_signal)));                 % paso de tiempo
     tf = (TotalTime-(TotalTime/length(Rx_signal)));       % tiempo final
     t = 0:dt:tf;                                  % definicion del v. tiempo
     assignin('base', 't_frame', t);
     assignin('base', 'x_frame', Rx_signal);
     %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 9. MATCHED FILTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
     mf_xPNRZ = conv(pulse_obj.pbase,Rx_signal');
     mf_xPNRZ = mf_xPNRZ./(max(abs(mf_xPNRZ)));
     assignin('base', 'mf_xPNRZ', mf_xPNRZ);
     %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 10. MUESTREO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
     % Sampler:
     mf_xPNRZ_max = max(mf_xPNRZ(1,1:50));
     sampling_st = find(abs(mf_xPNRZ)>= mf_xPNRZ_max ,1,'first');
     sampling_st = sampling_st + 1;
     sample = zeros(1,numel(mf_xPNRZ));
     sample(sampling_st:pulse_obj.mp:end) = 1;
     sampled_data = sample.*mf_xPNRZ;
     assignin('base', 'sampled_data', sampled_data);
     %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 11. DECODIFICACION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
     decode_start = 1;
     decode_received_bits = sampled_data(sampling_st:pulse_obj.mp:end);  
     decode_received_bits = decode_received_bits(:);
     decode_received_bits(decode_received_bits >= 0) = 1;
     decode_received_bits(decode_received_bits < 0) = 0;
     %decode_received_bits = decode_received_bits(decode_start:numel(decode_received_bits)-(numel(decode_received_bits)-(2097356))+(decode_start-1));

     [pre_st pre_end] = ftools_obj.preamble_detect(decode_received_bits);
     preamble_bits = decode_received_bits(pre_st:pre_end)';
     ftools_obj = setReceivedFrameSection(ftools_obj, "PREAMBLE", preamble_bits);
     SFD_bits = decode_received_bits(ftools_obj.preamble_size+1:ftools_obj.preamble_size+ftools_obj.SFD_size)';
     ftools_obj = setReceivedFrameSection(ftools_obj, "SFD", SFD_bits);
     DSA_bits = decode_received_bits(ftools_obj.preamble_size+ftools_obj.SFD_size+1:ftools_obj.preamble_size+ftools_obj.SFD_size+ftools_obj.DSA_size)';
     ftools_obj = setReceivedFrameSection(ftools_obj, "DSA", DSA_bits);
     HEADER_bits = decode_received_bits(ftools_obj.preamble_size+ftools_obj.SFD_size+ftools_obj.DSA_size+1:ftools_obj.preamble_size+ftools_obj.SFD_size+ftools_obj.DSA_size+ftools_obj.HEADER_size)';
     ftools_obj = setReceivedFrameSection(ftools_obj, "HEADER", HEADER_bits);
     assignin('base', 'frame_received', ftools_obj);

     DSA = ftools_obj.interpretDSA(DSA_bits)
     fprintf("DSA = %i\n");
     disp(DSA);
     WIDTH =  ftools_obj.interpretHeader(HEADER_bits', 0);
     HEIGHT =  ftools_obj.interpretHeader(HEADER_bits', 1);
     fprintf("width = %i\n", WIDTH);
     fprintf("height = %i\n", HEIGHT);

     assignin('base', 'received_bits', numel(decode_received_bits));
     assignin('base', 'WIDTH', WIDTH);
     assignin('base', 'HEIGHT', HEIGHT);
     assignin('base', 'DSA_MSG', DSA);
     assignin('base', 'decode_received_bits', decode_received_bits);
     if(sync == 0)
          PAYLOAD_bits = decode_received_bits(pre_end+ftools_obj.SFD_size+ftools_obj.DSA_size+ftools_obj.HEADER_size+1:numel(decode_received_bits));
          ftools_obj = setReceivedFrameSection(ftools_obj, "PAYLOAD", uint8(PAYLOAD_bits));
          assignin('base', 'frame_received', ftools_obj);
     else
          %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 10. SINCRONIZADOR ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
          method = 'Early-Late (non-data-aided)';
          symbolSync = comm.SymbolSynchronizer('TimingErrorDetector', method, 'SamplesPerSymbol', pulse_obj.mp);
          rxSym = symbolSync(mf_xPNRZ');
          rxSym = double(rxSym');
          release(symbolSync);
          %{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 11. DECODIFICACION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
          %bits2Tx = [preamble; SFD; DSA; header];
          decode_start = 7;
          decode_received_bits_sync = rxSym;
          decode_received_bits_sync = decode_received_bits_sync(:);
          decode_received_bits_sync(decode_received_bits_sync > 0) = 1;
          decode_received_bits_sync(decode_received_bits_sync < 0) = 0;

          if(numel(decode_received_bits_sync) > 2097356)
               decode_received_bits_sync = decode_received_bits_sync(decode_start:numel(decode_received_bits_sync)-(numel(decode_received_bits_sync)-(2097356))+(decode_start-1));
               PAYLOAD_bits = decode_received_bits_sync(pre_end+ftools_obj.SFD_size+ftools_obj.DSA_size+ftools_obj.HEADER_size+1:numel(decode_received_bits_sync));
          else
               decode_received_bits_sync = decode_received_bits_sync(decode_start:numel(decode_received_bits_sync)-(decode_start-1));
          end  
          index_start = search([1;0;1;0;0;0;1;0;1;0;1;0;0;0;1;0;1;0;1;0;0;0;1;0;1;0;1;0;0;0;1;0;1],decode_received_bits_sync);
     
          PAYLOAD_bits = decode_received_bits_sync(index_start:numel(decode_received_bits_sync)); %(815329:numel(decode_received_bits_sync) %045329
          %decode_received_bits_sync(pre_end+ftools_obj.SFD_size+ftools_obj.DSA_size+ftools_obj.HEADER_size+1:numel(decode_received_bits_sync)) = PAYLOAD_bits;
          %PAYLOAD_bits = decode_received_bits_sync(pre_end+ftools_obj.SFD_size+ftools_obj.DSA_size+ftools_obj.HEADER_size+1:numel(decode_received_bits_sync));
          ftools_obj = setReceivedFrameSection(ftools_obj, "PAYLOAD", uint8(PAYLOAD_bits));
          assignin('base', 'decode_received_bits', decode_received_bits_sync);
          assignin('base', 'frame_received', ftools_obj);
     end

     assignin('base', 'received_bits', numel(decode_received_bits));

     img_received = bitstoimage(PAYLOAD_bits);
     assignin('base', 'img_received', img_received);
     toc
 end