classdef communication_tools
    methods
        function ber = error_estimate(obj, bits_transmitted, bits_received)
            global bits_size
            ber = biterr(bits_transmitted,bits_received)/12176;
            ber = ber*100;
        end
    end
end