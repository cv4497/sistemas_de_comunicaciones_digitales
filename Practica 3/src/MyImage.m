classdef MyImage
    properties
        height
        width
        bits
    end
    methods
        function obj = MyImage(img_name)
            if(img_name == "lena512.mat")
                % cargar imagen de lena
                load 'lena512.mat';
                img = uint8(lena512);
                % convertir imagen a binario
                b=de2bi(uint8(img), 8,'left-msb');
                % obtener vector de bits concatenado
                obj.bits=b';
                obj.bits=obj.bits(:);
                assignin('base', 'bits_img', obj.bits);
                obj.width = numel(lena512(1,:));
                obj.height = numel(lena512(2,:));
            end
        end
        function obj = setFrameSection(obj, frame_section, data)
            if(frame_section == "PREAMBLE")
                obj.PREAMBLE = data';    
            elseif(frame_section == "SFD")
                obj.SFD = data';     
            elseif(frame_section == "DSA")
                obj.DSA = de2bi(uint8(data),8,'left-msb');
                obj.DSA = reshape(obj.DSA',1,numel(obj.DSA));
                obj.DSA = obj.DSA';
            elseif(frame_section == "HEADER")
                data_size = de2bi(size(data),16,'left-msb');
                obj.HEADER = [data_size(1,:) data_size(2,:)]';
            elseif(frame_section == "PAYLOAD")
                obj.PAYLOAD=de2bi(data,8,'left-msb');    % convertir imagen a binario
                obj.PAYLOAD=obj.PAYLOAD';              % calcular la transpuesta del vector
                obj.PAYLOAD=obj.PAYLOAD(:);            % obtener vector de bits concatenado
            else 
                %do nothing
            end
        end
        function wbits = binaryWidth(obj)
            wbits = de2bi(obj.width,'left-msb');
        end
        function hbits = binaryHeight(obj)
            hbits = de2bi(obj.height,'left-msb');
        end
        function frame_bits = getBits2Transmit(obj)
            frame_bits = [obj.PREAMBLE; obj.SFD; obj.DSA; obj.HEADER; obj.PAYLOAD];
        end
    end
end