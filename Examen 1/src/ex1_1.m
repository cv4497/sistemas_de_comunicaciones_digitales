%{
   Autor: Cesar Villarreal Hernandez
   Fecha: 10/19/2020
   Examen I: pregunta 1.
%}

clear; clc; close all;
mp = 11;    % muestras por pulso 
Fs = 57600; % frecuencia de muestreo
rs = mp/Fs;
s1 = 1*ones(1,mp);
tp = 1/57600;
pot = 1;
filter_order = 100;

samples = [0.043, 0.1350, 0.3240, 0.6060, 0.8820, 1.0000, 0.8820, 0.6060, 0.3240, 0.1350, 0.0430];
p_base = samples.*ones(1,mp);
% normalizacion del pulso base 
p_base = p_base/sqrt(energy_calc(p_base,Fs));
% pulso base normalizado
p_base = p_base*sqrt(1/(3*rs));

fprintf("p_base energy %.2f\n\r", energy_calc(p_base,Fs));
stem(p_base);

%wvtool(p_base);


%{
    I. Conversion de pixeles de la imagen a bits 
%}

bits = lena2bits();
bits_size = numel(bits);

%{
    II. Mapeo de bit a pulso
%}
%   c. Bipolar NRZ
xBPNRZ = bipolarNRZ(mp, bits, Fs, pot, "off");
showPulseTrain(xBPNRZ, mp, 200, 1, "bipolar", "off");
xlim([0, 200]);

%{ 
    III. Canal de comunicacion
%}
ch1 = channelFilter(filter_order, mp, 0.130,"hide");
% Filtrado
ch1_xBPNRZ = setCommunicationChannel(xBPNRZ,ch1);

%{
  Espectro de potencia de la señal recibida
%}
figure;
subplot(2,1,1);
pulseTrainPSD(xBPNRZ,Fs)
subplot(2,1,2);
pulseTrainPSD(ch1_xBPNRZ,Fs)

figure;
subplot(2,1,1);
showPulseTrain(xBPNRZ, mp, 200, filter_order, "bipolar", "off");
subplot(2,1,2);
showPulseTrain(ch1_xBPNRZ, mp, 200, filter_order, "filtered bipolar ch1", "on");
%wvtool(ch1)

%% IV. Recuperacion de los bits
received_bits_ch1_xBPNRZ = quantify(ch1_xBPNRZ, mp, filter_order, Fs)
fprintf("BER BPNRZ ch1 = %.2f \r\n", error_estimate(bits,received_bits_ch1_xBPNRZ));

figure;
subplot(2,1,1);
quantify_received_data(bits);
title('Original Picture');
subplot(2,1,2);
quantify_received_data(received_bits_ch1_xBPNRZ);
title('Bipolar NRZ Channel 1 Picture, fc = 0.130');

function bits = lena2bits()
    snip_size = 32;
    im_width = 350;
    im_height = im_width - ((snip_size*2)+1);

    % cargar imagen de lena
    load 'lena512.mat';
    % recortar imagen
    lenarec=lena512((im_height-snip_size):im_height,(im_width-snip_size):im_width);
    % convertir imagen a binario
    b=de2bi(lenarec,8,'left-msb');
    % calcular la transpuesta del vector
    b=b';
    % obtener vector de bits concatenado
    bits=b(:);
    newBits = reshape(bits,[8,1089]);
    % % calcular la transpuesta del vector de bits (1089 x 8)
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamaño del vector (33 x 33)
     newLena = reshape(newLena,[33,33]);
    % mostrar imagen 
    imshow(uint8(newLena));
end

function UNRZ = unipolarNRZ(mp, bits, Fs, pot, display_st)
    rs = Fs/mp;
    unrz = 1*ones(1,mp);
    % normalizacion de la potencia a 1 W
    unrz = unrz/sqrt(energy_calc(unrz,Fs));
    unrz = unrz*sqrt(1/rs);

    if(display_st == "on")
        fprintf("displaying pulse base in wvtool\n\r");
        wvtool(pulso_base);
    else
        fprintf("pulse base display disabled\n\r");
    end

    % generar tren de impulsos (1)
    s = zeros(1,numel(bits)*mp);
    % generar tren de impulsos (2)
    s(1:mp:end) = bits;    
    % generar tren de pulsos
    UNRZ = conv(unrz,s);
end

function PNRZ = polarNRZ(mp, bits, Fs, pot, display_st)
    s1 = bits;
    rs = Fs/mp;
    % analisis de potencia de la senal por pulso
    pnrz = 1*ones(1,mp);
    pnrz = pnrz/sqrt(energy_calc(pnrz,Fs));
    pnrz = pnrz*sqrt(1/rs);

    %pnrz = pnrz/sqrt(energy_calc(pnrz,Fs));
    %pnrz = pnrz*sqrt(0.5)
    s1(s1==0) = -1; 
    s = zeros(1,numel(s1)*mp);
    s(1:mp:end) = s1;
    PNRZ = conv(pnrz,s);
end

function BPNRZ = bipolarNRZ(mp, bits, Fs, pot, display_st)
    s1 = bits;
    rs = Fs/mp;
    % analisis de potencia de la senal por pulso
    samples = [0.043, 0.1350, 0.3240, 0.6060, 0.8820, 1.0000, 0.8820, 0.6060, 0.3240, 0.1350, 0.0430];
    bpnrz = samples.*ones(1,mp);
    
    last_i = 1;
    for i = 1:size(s1,1)
        if(s1(i) == 1)
            % Alternar entre 1 y -1
            last_i  = last_i*-1;
            s1(i) = last_i;
        end
    end
    
    s = zeros(1,numel(s1)*mp);
    s(1:mp:end) = s1;
    BPNRZ = conv(bpnrz,s);
    BPNRZ = abs(BPNRZ/sqrt(power_calc(BPNRZ)));
end

function MCHSTR = manchester(mp, bits, Fs, pot, display_st)
    pm = [ones(1,mp/2),-1*ones(1,mp/2)];

    if(display_st == "on")
        fprintf("displaying pulse base in wvtool\n\r");
        wvtool(pm);
    else
        fprintf("pulse base display disabled\n\r");
    end

    s1 = bits;
    s1(s1==0) = -1;
    s = zeros(1,(numel(bits)-1)*mp+1);
    s(1:mp:end) = s1;
    MCHSTR = conv(pm,s);
end

function lpf = channelFilter(filter_order, mp, fc, fv_disp)
    fprintf("fc = %.4f\n\r", fc);
    fprintf("filter order = %.1f\n\r", filter_order);
    % vector de frecuencia del filtro pasabajas con fc = 0.8 rad/muestra 
    f = [0 fc fc 1];
    % vector de magnitud del del filtro pasabajas con fc = 0.8 rad/muestra
    m = [1 1 0 0];
    lpf = fir2(filter_order, f, m);
    if(fv_disp == "show")
        fprintf("Displaying fvtool graph\n\r");
        fvtool(lpf);
    else 
        fprintf("fvtool graph not shown\n\r");
    end
end 

function filt_pt = setCommunicationChannel(xPulse,filter)
    filt_pt = conv(xPulse,filter);
end

function showPulseTrain(xPulse, mp, n_pulses, filter_order, graph_title, channel_st)
    if(channel_st == "on")
        stem(xPulse((filter_order/2):mp*n_pulses+filter_order/2),'r')
        title(strcat(graph_title," pulse train"));;
        xlabel('samples(n)');
        ylabel('impulse d(n)');
        xlim([0,200])
    else
        stem(xPulse(1:mp*n_pulses),'r');
        title(strcat(graph_title," pulse train"));
        xlabel('samples(n)');
        ylabel('impulse d(n)');
        xlim([0,200])
    end
end

function pulseTrainPSD(xPulse,Fs)
    % graficar densidad espectral de potencia 
    pwelch(xPulse, [], [], [], Fs,'power');  
end

%{
    Receptor filter for UNRZ,PNRZ,BPNRZ line encoding
%}
function receivedBits = quantify(xPulse, mp, filter_order, Fs)
    global bits_size
    xPulse(xPulse > 1) = 1;
    xPulse(xPulse < 0) = 0;

    test = xPulse(filter_order/2+mp/2:mp:end-(filter_order/2));
    roundedTest = round(test);
    roundedTest(roundedTest < 0) = roundedTest(roundedTest<0)+1;
    receivedBits = roundedTest';
    receivedBits = receivedBits(1:bits_size);
end

%{
    Receptor filter for the manchester line encoding
%}
function receivedBits = quantify_m(xPulse, mp, filter_order, Fs)
    global bits_size
    xPulse(xPulse > 0) = 1;
    xPulse(xPulse < 0) = 0;

    test = xPulse(filter_order/2+mp/2:mp:end-(filter_order/2));
    roundedTest = round(test);
    roundedTest(roundedTest < 0) = roundedTest(roundedTest<0)+1;
    receivedBits = roundedTest';
    receivedBits = receivedBits(1:bits_size);
end

function quantify_received_data(receivedBits)
    %{
    % reasignar el tamano del vector (8 x 1089)
    newBits = reshape(receivedPulse,[8,1089]);
    % calcular la transpuesta del vector de bits (1089 x 8)
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamano del vector (33 x 33)
    newLena = reshape(newLena,[33,33]);
    %} 
    newBits = reshape(receivedBits,[8,1089]);
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamano del vector (33 x 33)
    newLena = reshape(newLena,[33,33]);
    % mostrar imagen 
    imshow(uint8(newLena));
end

function pow = power_calc(xPulse)
    pow = (1/numel(xPulse))*sum(xPulse*xPulse');
end

function energy = energy_calc(xPulse,Fs)
    energy = (1/Fs)*sum(xPulse);
end

function ber = error_estimate(bits_transmitted,bits_received)
    global bits_size
    ber = biterr(bits_transmitted,bits_received)/bits_size;
    ber = ber*100;
end


%{
    Conclusiones:
    puedo afirmar que se obtuvo correctamente la imagen, la frecuencia de corte del filtro es ideal para la transmision y recepción de la imagen.
}
