% cerrar todas las ventanas y eliminar workspace
close all; clc; clear;

file_name = 'chirp.wav';         
[x, t, Fs, dt, tf, TotalTime] = sample_audiofile(file_name,16);
disp(Fs);
% gr�fica de la se�al muestreada
figure(1);
%plot(t,x,'r'); 

% espectro de potencia de la se�al muestreada
figure;
pwelch(x, [0 10], [], [], Fs,'power'); 

function [xq,t,Fs, dt, tf, TotalTime] = sample_audiofile(file_name,k)
    [X, Fs] = audioread(file_name);
    X = X(:,1);
    
    % c�lculo del vector de tiempo
    TotalTime = length(X)./Fs;                    % duraci�n del audio
    dt = (TotalTime/(length(X)));                 % paso de tiempo
    tf = (TotalTime-(TotalTime/length(X)));       % tiempo final
    t = 0:dt:tf;                                  % definici�n del v. tiempo
    
    % se�al muestreada con sample-and-hold
    ts = 2;
    xs = zeros(1,length(t));
    
    for i=1:length(t)
    if(rem(i,ts)==1)
    tmp = X(i);
    end
    xs(i) = tmp;
    end

    % cuantificaci�n
    M = 2^k;
    fprintf("k = %i\n", k);

    int = (max(xs)-min(xs))/M;
    m = (min(xs)+int/2):int:(max(xs)-int/2);
    xq = zeros(1,length(t));
    
    for i=1:length(t)
        [tmp k] = min(abs(xs(i)-m));
        xq(i) = m(k);
    end
end


function spectrogram_audio_vector(X, Fs, title_graph)
    figure;
    spectrogram(X,2000,0,5000,Fs,'yaxis');        % gr�fica del espectrograma
    colormap jet
    xlim([0 10]);                                  % limites del eje x
    ylim([0 15]);                                  % limites del eje y
    xticks(0:0.5:10);                              % paso del eje x
    title('Espectrograma');                       % t�tulo de la gr�fica
    xlabel('Tiempo (segundos)');                  % nombre del eje x
    ylabel('Frecuencia (kHz)');                    % nombre del eje y
end