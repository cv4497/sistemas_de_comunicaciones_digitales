Fs = 96e3;          % Samples per second
Ts = 1/Fs;          % Periodo de muestreo
beta = 0.20;         % Factor de Roll-off
Rb = 12000;  % Bit rate = Baud rate
mp = 8;    % Muestras por pulso
display(mp);
Tp = 1/Rb;          % Symbol period
B = (Rb*(1+beta)/2); % Ancho de banda consumido
D = 6;              % Duracion de tiempo en terminos de tp
type = 'srrc';      % Forma del pulso: Square Root Rise Cosine
E = Tp;             % Energia
[pbase ~] = rcpulse(beta, D, Tp, Ts, type, E); % Generacion del pulso
fprintf("Fs = %i \n\r", Fs);
fprintf("Rb = %i bps\n\r", Rb);
fprintf("mp = %i \n\r", mp);