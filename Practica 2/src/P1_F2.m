clear; clc; close all;
global header;
global size_img;
pt_eye = 1;
sound_enabled = 0;
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{
     Autor: Cesar Villarreal Hernandez
     Titulo: Practica I
     Descripcion: Fase II
     Asignatura: Sistemas de Comunicaciones Digitales
     Fecha: 11/noviembre/2020
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~1. PARAMETROS DEL SISTEMA ~~~~~~~~~~~~~~~~~~~~~~~~ %}
display_info();
% muestras por pulso 
mp = 10;                     
% frecuencia de muestreo
Fs = 96000;
% periodo de muestreo
Ts = 1/Fs;                   
% generar un solo pulso
R = Fs/mp;
impulse_file = 'IMPULSE_RESPONSE.wav';
frame_file = 'FRAME_7200.wav';
 
if(frame_file == 'FRAME_1800.wav')
    decode_start = 3;
    beta = 0.5;        % Factor de Roll-off
    Rb = 2400;         % Bit rate = Baud rate
    mp = 40;             % Muestras por pulso
else
    decode_start = 1;
    beta = 0.5;        % Factor de Roll-off
    Rb = 9600;         % Bit rate = Baud rate
    mp = 10;             % Muestras por pulso
end


%{ ~~~~~~~~~~~~~~~~~~~~~~~~2. CONSTRUCCION DEL FRAME ~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ PAYLOAD: Conversion de una imagen a bits  %}
load 'lena512.mat';                 % cargar imagen de lena
img = uint8(lena512);               % conversion a 8-bits
img = img(248:280,245:289,1);       % recorte de imagen a 33x45 
%imshow(img);                       % mostrar imagen
payload=de2bi(img,8,'left-msb');    % convertir imagen a binario
payload=payload';                   % calcular la transpuesta del vector
payload=payload(:);                 % obtener vector de bits concatenado

%{ trama de sincronizacion (preamble) %}
preamble = [1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0]';
%{ Delimitador inicial de la trama de bits (SFD)  %}
SFD = [1 0 1 0 1 0 1 1]';
%{ Direccion del destino (DSA) %}
DSA = de2bi(uint8('Practica 1 FASE II: cesar'),8,'left-msb');
DSA = reshape(DSA',1,numel(DSA));
DSA = DSA';
%{ Construccion del encabezado (header)  %}
size_img=de2bi(size(img),16,'left-msb');
header = [size_img(1,:) size_img(2,:)]';

%{ Construccion del frame  %}
bits2Tx = [preamble; SFD; DSA; header;payload];
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~ 3. GENERACION DEL PULSO BASE ~~~~~~~~~~~~~~~~~~~~~~~ %}
Fs = 96e3;          % Samples per second
Ts = 1/Fs;          % Periodo de muestreo
display(mp);
Tp = 1/Rb;          % Symbol period
B = (Rb*(1+beta)/2); % Ancho de banda consumido
D = 6;              % Duracion de tiempo en terminos de tp
type = 'srrc';      % Forma del pulso: Square Root Rise Cosine
E = Tp;             % Energia
[pbase ~] = rcpulse(beta, D, Tp, Ts, type, E); % Generacion del pulso
fprintf("Fs = %i \n\r", Fs);
fprintf("Rb = %i bps\n\r", Rb);
fprintf("mp = %i \n\r", mp);
fprintf("B = %i \n\r", B);
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~ 4. GENERACION DE CODIGO DE LINEA POLAR NRZ ~~~~~~~~~~~~~~~~ %}
xPNRZ = polarNRZ(bits2Tx, pbase, mp, Fs);
xPNRZ = xPNRZ./(max(abs(xPNRZ)));
figure; 
showPulseTrain(xPNRZ, mp, numel(bits2Tx), 0, "polar", "off");

%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~ 5. GENERACION DE DIAGRAMA DE OJO PARA EL TREN DE PULSOS ~~~~~~~~~~ %}
if(pt_eye == 1)
figure;
ed1 = comm.EyeDiagram('SampleRate',Fs*mp,'SamplesPerSymbol',mp);
delay= round(numel(pbase)/2);
ed1(xPNRZ(delay+1:end-delay)') % xpnrz es el tren de pulsos Tx polar NRZ

figure;
%showPulseTrain(xPNRZ, mp, numel(xPNRZ), 0, "polar", "off");
title('Espectro del tren de pulsos');
end
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~ 6. TRANSMISION DEL PULSO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
if(sound_enabled == 1)
soundsc([zeros(1,Fs/2) xPNRZ],Fs);
end
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~ 7. GRAFICA DE LA SENAL RECIBIDA~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%[x_frame, t, Fs, dt, tf, TotalTime] = sample_audiofile(frame_file,20);
[x_frame, Fs] = audioread(frame_file);
x_frame = x_frame(:,1);

%{ calculo del vector de tiempo %}
TotalTime = length(x_frame)./Fs;                    % duracion del audio
dt = (TotalTime/(length(x_frame)));                 % paso de tiempo
tf = (TotalTime-(TotalTime/length(x_frame)));       % tiempo final
t = 0:dt:tf;                                  % definicion del v. tiempo

%{ grafica de la senal muestreada %}
figure;
plot(t,x_frame,'r'); 
title('Grafica del frame recibido');
xlabel('tiempo'); ylabel('amplitud');
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~ 8. QUITAR SILENCIO AL FRAME ~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
Fs=96e3; sec=5; % Time duration of the whole communication including the silence
Rx_signal = x_frame;
threshold = 0.1; % Detecting the channel energization
start = find(abs(Rx_signal)> threshold,3,'first'); % Initial
stop = find(abs(Rx_signal)> threshold,1,'last'); % End
Rx_signal = Rx_signal (start:stop);
figure;
plot(t(1:numel(Rx_signal)),Rx_signal,'r'); 
title('Grafica del frame recibido sin silencio');
xlabel('tiempo'); ylabel('amplitud');
% espectro de potencia de la senal muestreada
figure;
pwelch(Rx_signal, [], [], [], Fs,'power'); 
title('Espectro de potencia del frame');
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 9. MATCHED FILTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
mf_xPNRZ = conv(pbase,Rx_signal');
mf_xPNRZ = mf_xPNRZ./(max(abs(mf_xPNRZ)));
figure;
showPulseTrain(mf_xPNRZ, mp, 200, 0, "polar", "off");
title('Grafica del frame filtrado con Match filter');
figure;
pwelch(mf_xPNRZ, [], [], [], Fs,'power'); 
title('Espectro de potencia del frame filtrado con Match filter');
ed2 = comm.EyeDiagram('SampleRate',Fs*mp,'SamplesPerSymbol',mp);
delay= round(numel(pbase)/2);
figure;
ed2(mf_xPNRZ(delay+1:end-delay)') % xpnrz es el tren de pulsos Tx polar NRZ

method = 'Early-Late (non-data-aided)';
symbolSync = comm.SymbolSynchronizer('TimingErrorDetector', method, 'SamplesPerSymbol', mp);
rxSym = symbolSync(mf_xPNRZ');
release(symbolSync);

%{
M = 2;
refC = qammod(0:M-1,M);
constdiag = comm.ConstellationDiagram('ReferenceConstellation',refC, ...
'XLimits',[-2 2],'YLimits',[-2 2]);
constdiag(mf_xPNRZ');
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 10. MUESTREO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% Sampler:

mf_xPNRZ_max = max(mf_xPNRZ(1,1:50));
sampling_st = find(abs(mf_xPNRZ)>= mf_xPNRZ_max ,1,'first');
sampling_st = sampling_st + 1;
sample = zeros(1,numel(mf_xPNRZ));
sample(sampling_st:mp:end) = 1;
sampled_data = sample.*mf_xPNRZ;

M = 2;
refC = qammod(0:M-1,M);
constdiag2 = comm.ConstellationDiagram('ReferenceConstellation',refC, ...
'XLimits',[-2 2],'YLimits',[-2 2]);
constdiag2(sampled_data');

rxSym = double(rxSym');


%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 11. DECODIFICACION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%bits2Tx = [preamble; SFD; DSA; header];
decode_received_bits = sampled_data(sampling_st:mp:end);  
decode_received_bits = decode_received_bits(:);
decode_received_bits(decode_received_bits >= 0) = 1;
decode_received_bits(decode_received_bits < 0) = 0;

decode_received_bits = decode_received_bits(decode_start:numel(decode_received_bits)-(numel(decode_received_bits)-numel(bits2Tx))+(decode_start-1));

preamble_bits = decode_received_bits(1:numel(preamble));
SFD_BITS = decode_received_bits(numel(preamble)+1:numel(preamble)+numel(SFD));
DSA_BITS = decode_received_bits(numel(preamble)+numel(SFD)+1:numel(preamble)+numel(SFD)+numel(DSA));
HEADER_BITS = decode_received_bits(numel(preamble)+numel(SFD)+numel(DSA)+1:numel(preamble)+numel(SFD)+numel(DSA)+numel(header));
PAYLOAD = decode_received_bits(numel(preamble)+numel(SFD)+numel(DSA)+numel(header)+1:numel(preamble)+numel(SFD)+numel(DSA)+numel(header)+numel(payload));

disp("error bits =");
disp(error_estimate(bits2Tx,decode_received_bits));

disp("error preamble bits =");
disp(error_estimate(preamble,preamble_bits));

disp("error SFD bits =");
disp(error_estimate(SFD,SFD_BITS));

disp("error DSA bits =");
disp(error_estimate(DSA,DSA_BITS));

disp("error HEADER bits =");
disp(error_estimate(header,HEADER_BITS));

disp("error PAYLOAD bits =");
disp(error_estimate(payload,PAYLOAD));

lena_received = bits_to_image(PAYLOAD);

figure;
subplot(2,1,1);
imshow(uint8(img));
title('imagen transmitida');
subplot(2,1,2);
imshow(uint8(lena_received));
title('imagen recibida');
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}

function ber = error_estimate(bits_transmitted,bits_received)

global bits_size

ber = biterr(bits_transmitted,bits_received)/numel(bits_transmitted);
     ber = ber*100;
 end

 function new_mat = bits_to_image(receivedBits)
     global header;
     global size_img;
     receivedBits =  uint8(receivedBits);

     bits_to_matrix = vec2mat(receivedBits,8);
     lena_matrix = bi2de(bits_to_matrix,'left-msb');
     new_mat = (vec2mat(lena_matrix,33))';
     figure; 
     imshow(uint8(new_mat));
 end
 