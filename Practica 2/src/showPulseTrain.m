function showPulseTrain(xPulse, mp, n_pulses, filter_order, graph_title, channel_st)
    if(channel_st == "on")
        plot(xPulse((filter_order/2):mp*n_pulses+filter_order/2),'r')
        title(strcat(graph_title," pulse train"));;
        xlabel('samples(n)');
        ylabel('impulse d(n)');
        %xlim([0,5000])
    else
        plot(xPulse(1:mp*n_pulses),'r');
        title(strcat(graph_title," pulse train"));
        xlabel('samples(n)');
        ylabel('impulse d(n)');
    end
end