close all; clc;
% frecuencia de muestreo
fs = 96000;
% periodo de muestreo
ts = 1/fs;
%tiempo final
tf = 5;
% tiempo inicial
ti = 0;
% vector de tiempo
t = ti:ts:tf-ti;
% generacion de ruido gaussiano
n = sqrt(1)*randn(1,length(t));
% reproduccion del ruido al canal
soundsc(n,fs);

figure;
pwelch(n, [], [], [], Fs,'power'); 
