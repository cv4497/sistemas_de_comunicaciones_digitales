# ***Repositorio Personal SCD Otoño 2020***

**Desarrollador:**
- César Villarreal ie707560 @cv4497

**Descripción del repositorio:** 
- Este repositorio contiene las tareas individuales de la clase de Sistemas de Comunicaciones Digitales del semestre Otoño 2020.

**Índice**
- [Práctica 1](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Practica%201)
- [Práctica 2](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Practica%202)
- [Tarea 4](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Tarea%204)
- [Tarea 5](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Tarea%205)
- [Tarea 6](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Tarea%206)
- [Exámen 1](https://gitlab.com/cv4497/sistemas_de_comunicaciones_digitales/-/tree/master/Examen%201)