clear; clc; close all;
%% I. Conversion de pixeles de la imagen a bits
% muestras por pulso 
mp = 10;                     
% frecuencia de muestreo
Fs = 96000;
% periodo de muestreo
Ts = 1/Fs;                   
% generar un solo pulso
R = Fs/mp;

snip_size = 32;
im_width = 350;
im_height = im_width - ((snip_size*2)+1);

% cargar imagen de lena
load 'lena512.mat';
% recortar imagen
lenarec=lena512((im_height-snip_size):im_height,(im_width-snip_size):im_width);
% convertir imagen a binario
b=de2bi(lenarec,8,'left-msb');
% calcular la transpuesta del vector
b=b';
% obtener vector de bits concatenado
bits=b(:);

%% II. Mapeo de c�digo de l�nea

filter_order = 100; % orden del filtro

% vector de frecuencia del filtro pasabajas con fc = 0.8 rad/muestra 
f = [0 0.05 0.05 1];
% vector de magnitud del del filtro pasabajas con fc = 0.8 rad/muestra
m = [1 1 0 0];
% dise�o de filtro FIR pasabajas de orden 100
f0_05 = fir2(filter_order, f, m);
%fvtool(f0_05);
% vector de frecuencia del filtro pasabajas con fc = 0.8 rad/muestra 
f = [0 0.2 0.2 1];
% vector de magnitud del del filtro pasabajas con fc = 0.8 rad/muestra
m = [1 1 0 0];
% dise�o de filtro FIR pasabajas de orden 100
f0_2 = fir2(filter_order, f, m);
%fvtool(f0_2);

% vector de frecuencia del filtro pasabajas con fc = 0.8 rad/muestra 
f = [0 0.4 0.4 1];
% vector de magnitud del del filtro pasabajas con fc = 0.8 rad/muestra
m = [1 1 0 0];
% dise�o de filtro FIR pasabajas de orden 100
f0_4 = fir2(filter_order, f, m);
% mostrar la gr�fica del filtro pasabajas de orden 100 y fc = 0.8      
%fvtool(f0_4);

%% Unipolar NRZ
% analisis de potencia de la se�al por pulso
pbase_p = rectwin(mp);% wvtool(pnrz);                 
% generar tren de impulsos (1)
s = zeros(1,numel(bits)*mp);  
% generar tren de impulsos (2)
s(1:mp:end) = bits;           
% generar tren de pulsos
xUNRZ = conv(pbase_p,s);

xUNRZ_filt_0_05 = conv(xUNRZ,f0_05);
xUNRZ_filt_0_2 = conv(xUNRZ,f0_2);
xUNRZ_filt_0_4 = conv(xUNRZ,f0_4);
                               
subplot(4,1,1);
stem(xUNRZ(1:mp*16),'r');
title('unipolar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,2);
stem(xUNRZ_filt_0_05((filter_order/2):mp*16),'r')
title('filtered unipolar NRZ pulse train, fc = 0.05');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,3);
stem(xUNRZ_filt_0_2((filter_order/2):mp*16),'r')
title('filtered unipolar NRZ pulse train, fc = 0.2');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,4);
stem(xUNRZ_filt_0_4((filter_order/2):mp*16),'r')
title('filtered unipolar NRZ pulse train, fc = 0.4');
xlabel('samples(n)');
ylabel('impulse d(n)');

figure;
subplot(4,1,1);
% graficar densidad espectral de potencia
pwelch(xUNRZ, [], [], [], Fs,'power'); 
title('Espectro de potencia unipolar NRZ sin filtrar');
subplot(4,1,2);
pwelch(xUNRZ_filt_0_05, [], [], [], Fs,'power'); 
title('Espectro de potencia unipolar NRZ filtrado, fc = 0.05');
subplot(4,1,3);
pwelch(xUNRZ_filt_0_2, [], [], [], Fs,'power'); 
title('Espectro de potencia unipolar NRZ filtrado, fc = 0.2');
subplot(4,1,4);
pwelch(xUNRZ_filt_0_4, [], [], [], Fs,'power');
title('Espectro de potencia unipolar NRZ filtrado, fc = 0.4');

%% Polar NRZ
s1 = bits;
% analisis de potencia de la se�al por pulso
pnrz = 1*ones(1,mp);% wvtool(pnrz); 
% Convertir �0� en �-1�
s1(s1==0) = -1; 
s = zeros(1,numel(s1)*mp);
s(1:mp:end) = s1;
xPNRZ = conv(pnrz,s);

xPNRZ_filt_0_05 = conv(xPNRZ,f0_05);
xPNRZ_filt_0_2 = conv(xPNRZ,f0_2);
xPNRZ_filt_0_4 = conv(xPNRZ,f0_4);

% crear figura
figure;                                
subplot(2,1,1);
stem(xPNRZ(1:mp*16))
hold on;
stem(xPNRZ(1:mp*16),'LineStyle', 'none');
hold off;
title('polar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(2,1,2);
pwelch(xPNRZ,500,300,500,Fs,'power');

figure;                                
subplot(4,1,1);
stem(xPNRZ(1:mp*16),'r')
title('polar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,2);
stem(xPNRZ_filt_0_05((filter_order/2):mp*16),'r')
title('filtered polar NRZ pulse train, fc = 0.05');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,3);
stem(xPNRZ_filt_0_2((filter_order/2):mp*16),'r')
title('filtered polar NRZ pulse train, fc = 0.2');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,4);
stem(xPNRZ_filt_0_4((filter_order/2):mp*16),'r')
title('filtered polar NRZ pulse train, fc = 0.4');
xlabel('samples(n)');
ylabel('impulse d(n)');

figure;
subplot(4,1,1);
% graficar densidad espectral de potencia
pwelch(xPNRZ, [], [], [], Fs, 'power'); 
title('Espectro de potencia polar NRZ sin filtrar');
subplot(4,1,2);
pwelch(xPNRZ_filt_0_05, [], [], [], Fs, 'power'); 
title('Espectro de potencia polar NRZ filtrado, fc = 0.05');
subplot(4,1,3);
pwelch(xPNRZ_filt_0_2, [], [], [], Fs, 'power'); 
title('Espectro de potencia polar NRZ filtrado, fc = 0.2');
subplot(4,1,4);
pwelch(xPNRZ_filt_0_4, [], [], [], Fs, 'power');
title('Espectro de potencia polar NRZ filtrado, fc = 0.4');

%% Bipolar NRZ
s1 = bits;
% analisis de potencia de la se�al por pulso
bpnrz = 1*ones(1,mp);% wvtool(pnrz); 

last_i = 1;
for i = 1:size(s1,1)
    if(s1(i) == 1)
        % Alternar entre 1 y -1
        last_i  = last_i*-1;
        s1(i) = last_i;
    end
end

s = zeros(1,numel(s1)*mp);
s(1:mp:end) = s1;
xBPNRZ = conv(bpnrz,s);

xBPNRZ_filt_0_05 = conv(xBPNRZ,f0_05);
xBPNRZ_filt_0_2 = conv(xBPNRZ,f0_2);
xBPNRZ_filt_0_4 = conv(xBPNRZ,f0_4);

figure;                                
subplot(4,1,1);
stem(xBPNRZ(1:mp*16),'r')
title('bipolar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,2);
stem(xBPNRZ_filt_0_05((filter_order/2):mp*16),'r')
title('filtered bipolar NRZ pulse train, fc = 0.05');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,3);
stem(xBPNRZ_filt_0_2((filter_order/2):mp*16),'r')
title('filtered bipolar NRZ pulse train, fc = 0.2');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(4,1,4);
stem(xBPNRZ_filt_0_4((filter_order/2):mp*16),'r')
title('filtered bipolar NRZ pulse train, fc = 0.4');
xlabel('samples(n)');
ylabel('impulse d(n)');

figure;
subplot(4,1,1);
% graficar densidad espectral de potencia
pwelch(xBPNRZ, [], [], [], Fs,'power'); 
title('Espectro de potencia bipolar NRZ NRZ sin filtrar');
subplot(4,1,2);
pwelch(xBPNRZ_filt_0_05, [], [], [], Fs,'power'); 
title('Espectro de potencia bipolar NRZ filtrado, fc = 0.05');
subplot(4,1,3);
pwelch(xBPNRZ_filt_0_2, [], [], [], Fs,'power'); 
title('Espectro de potencia bipolar NRZ filtrado, fc = 0.2');
subplot(4,1,4);
pwelch(xBPNRZ_filt_0_4, [], [], [], Fs,'power');
title('Espectro de potencia bipolar NRZ filtrado, fc = 0.4');
% 
