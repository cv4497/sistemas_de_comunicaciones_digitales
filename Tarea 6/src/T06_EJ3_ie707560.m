clear; clc; close all;
global bits_size
global delay_total
global delay_rec
global delay_ch1
global delay_ch2
global delay_ch3

plot_pt = 1;
plot_psd = 1;
print_pow = 1;
ber_estimate = 1;

%% Parametros de referencia
xw = "60";  % digitos de expediente
mp = 10;    % muestras por pulso 
Fs = 96000; % frecuencia de muestreo
Ts = 1/Fs;  % periodo de muestreo
R = Fs/mp;  % generar un solo pulso
pot = 1;    % potencia de transmision
filter_order = 100;
channel_delay = filter_order/2;

%{
    I. Conversion de pixeles de la imagen a bits 
%}

bits = lena2bits();
bits_size = numel(bits);

%{
    II. Mapeo de bit a pulso
%}
%   a. Unipolar NRZ
    xUNRZ = unipolarNRZ(mp, bits, Fs, pot, "off");
%   b. Polar NRZ
    xPNRZ = polarNRZ(mp, bits, Fs, pot, "off");
%   c. Bipolar NRZ
    xBPNRZ = bipolarNRZ(mp, bits, Fs, pot, "off");
%   d. Manchester
    xMCHSTR = manchester(mp, bits, Fs, pot, "off");

%{ 
    III. Canal de comunicacion
%}
ch1 = channelFilter(filter_order, mp,str2num(strcat(num2str(0.30),xw)),"hide");
ch2 = channelFilter(filter_order, mp,str2num(strcat(num2str(0.15),xw)),"hide");
ch3 = channelFilter(filter_order, mp, str2num(strcat("0.0",xw)),"hide");

% Filtrado y normalizacion de potencia a 1 W
ch1_xUNRZ = setCommunicationChannel(xUNRZ,ch1);
ch2_xUNRZ = setCommunicationChannel(xUNRZ,ch2);
ch3_xUNRZ = setCommunicationChannel(xUNRZ,ch3);

ch1_xPNRZ = setCommunicationChannel(xPNRZ,ch1);
ch2_xPNRZ = setCommunicationChannel(xPNRZ,ch2);
ch3_xPNRZ = setCommunicationChannel(xPNRZ,ch3);

ch1_xBPNRZ = setCommunicationChannel(xBPNRZ,ch1);
ch2_xBPNRZ = setCommunicationChannel(xBPNRZ,ch2);
ch3_xBPNRZ = setCommunicationChannel(xBPNRZ,ch3);

ch1_xMCHSTR = setCommunicationChannel(xMCHSTR,ch1);
ch2_xMCHSTR = setCommunicationChannel(xMCHSTR,ch2);
ch3_xMCHSTR = setCommunicationChannel(xMCHSTR,ch3);

if(print_pow == 1)
fprintf("power UNRZ = %.2f W\r", power_calc(xPNRZ));
fprintf("power UNRZ ch1 = %.2f W\r", power_calc(ch1_xUNRZ));
fprintf("power UNRZ ch2 = %.2f W\r", power_calc(ch2_xUNRZ));
fprintf("power UNRZ ch3 = %.2f W\n\r", power_calc(ch3_xUNRZ));

fprintf("power xPNRZ = %.2f W\r", power_calc(xPNRZ));
fprintf("power PNRZ ch1 = %.2f W\r", power_calc(ch1_xPNRZ));
fprintf("power PNRZ ch2 = %.2f W\r", power_calc(ch2_xPNRZ));
fprintf("power PNRZ ch3 = %.2f W\n\r", power_calc(ch3_xPNRZ));

fprintf("power BPNRZ = %.2f W\r", power_calc(xBPNRZ));
fprintf("power BPNRZ ch1 = %.2f W\r", power_calc(ch1_xBPNRZ));
fprintf("power BPNRZ ch2 = %.2f W\r", power_calc(ch2_xBPNRZ));
fprintf("power BPNRZ ch3 = %.2f W\n\r", power_calc(ch3_xBPNRZ));

fprintf("power MCHSTR = %.2f W\r", power_calc(xMCHSTR));
fprintf("power MCHSTR ch1 = %.2f W\r", power_calc(ch1_xMCHSTR));
fprintf("power MCHSTR ch2 = %.2f W\r", power_calc(ch2_xMCHSTR));
fprintf("power MCHSTR ch3 = %.2f W\n\r", power_calc(ch2_xMCHSTR));
end

figure;
subplot(4,1,1);
showPulseTrain(xUNRZ, mp, 10, filter_order, "unipolar", "off");
subplot(4,1,2);
showPulseTrain(ch1_xUNRZ, mp, 10, filter_order, "filtered unipolar ch1", "on");
subplot(4,1,3);
showPulseTrain(ch2_xUNRZ, mp, 10, filter_order, "filtered unipolar ch2", "on");
subplot(4,1,4);
showPulseTrain(ch3_xUNRZ, mp, 10, filter_order, "filtered unipolar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xPNRZ, mp, 10, filter_order, "polar", "off");
subplot(4,1,2);
showPulseTrain(ch1_xPNRZ, mp, 10, filter_order, "filtered polar ch1", "on");
subplot(4,1,3);
showPulseTrain(ch2_xPNRZ, mp, 10, filter_order, "filtered polar ch2", "on");
subplot(4,1,4);
showPulseTrain(ch3_xPNRZ, mp, 10, filter_order, "filtered polar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xBPNRZ, mp, 10, filter_order, "bipolar", "off");
subplot(4,1,2);
showPulseTrain(ch1_xBPNRZ, mp, 10, filter_order, "filtered bipolar ch1", "on");
subplot(4,1,3);
showPulseTrain(ch2_xBPNRZ, mp, 10, filter_order, "filtered bipolar ch2", "on");
subplot(4,1,4);
showPulseTrain(ch3_xBPNRZ, mp, 10, filter_order, "filtered bipolar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xMCHSTR, mp, 10, filter_order, "manchester", "off");
subplot(4,1,2);
showPulseTrain(ch1_xMCHSTR, mp, 10, filter_order, "filtered manchester ch1", "on");
subplot(4,1,3);
showPulseTrain(ch2_xMCHSTR, mp, 10, filter_order, "filtered manchester ch2", "on");
subplot(4,1,4);
showPulseTrain(ch3_xMCHSTR, mp, 10, filter_order, "filtered manchester ch3", "on");

figure;
%pulseTrainPSD(ch1_xBPNRZ,Fs);
%{
  Espectro de potencia de la señal recibida
%}
if(plot_psd == 1)
figure;
subplot(4,1,1);
pulseTrainPSD(xUNRZ,Fs);
title('Unipolar power spectrum');
subplot(4,1,2);
pulseTrainPSD(ch1_xUNRZ,Fs);
title('Unipolar NRZ ch1 power spectrum');
subplot(4,1,3);
pulseTrainPSD(ch2_xUNRZ,Fs);
title('Unipolar NRZ ch2 power spectrum');
subplot(4,1,4);
pulseTrainPSD(ch3_xUNRZ,Fs);
title('Unipolar NRZ ch3 power spectrum');

figure;
subplot(4,1,1);
pulseTrainPSD(xPNRZ,Fs);
title('Polar NRZ power spectrum');
subplot(4,1,2);
pulseTrainPSD(ch1_xPNRZ,Fs);
title('Polar NRZ ch1 power spectrum');
subplot(4,1,3);
pulseTrainPSD(ch2_xPNRZ,Fs);
title('Polar NRZ ch2 power spectrum');
subplot(4,1,4);
pulseTrainPSD(ch3_xPNRZ,Fs);
title('Polar NRZ ch3 power spectrum');

figure;
subplot(4,1,1);
pulseTrainPSD(xBPNRZ,Fs);
title('Bipolar NRZ power spectrum');
subplot(4,1,2);
pulseTrainPSD(ch1_xBPNRZ,Fs);
title('Bipolar NRZ ch1 power spectrum');
subplot(4,1,3);
pulseTrainPSD(ch2_xBPNRZ,Fs);
title('Bipolar NRZ ch2 power spectrum');
subplot(4,1,4);
pulseTrainPSD(ch3_xBPNRZ,Fs);
title('Bipolar NRZ ch3 power spectrum');

figure;
subplot(4,1,1)
pulseTrainPSD(xMCHSTR,Fs);
title('Manchester power spectrum');
subplot(4,1,2);
pulseTrainPSD(ch1_xMCHSTR,Fs);
title('Manchester ch1 power spectrum');
subplot(4,1,3);
pulseTrainPSD(ch2_xMCHSTR,Fs);
title('Manchester ch2 power spectrum');
subplot(4,1,4);
pulseTrainPSD(ch3_xMCHSTR,Fs);
title('Manchester ch3 power spectrum');
end

%% V. Filtro Receptor
bp = basePulse(mp,Fs,"unipolar");
delay_rec = round(numel(bp)/2);
delay_total = filter_order/2 + delay_rec;
rf_ch1_xUNRZ = conv(bp,ch1_xUNRZ);
rf_ch2_xUNRZ = conv(bp,ch1_xUNRZ);
rf_ch3_xUNRZ = conv(bp,ch2_xUNRZ);

bp = basePulse(mp,Fs,"polar");
delay_rec = round(numel(bp)/2);
delay_total = filter_order/2 + delay_rec;
rf_ch1_xPNRZ = conv(bp,ch1_xPNRZ);
rf_ch2_xPNRZ = conv(bp,ch2_xPNRZ);
rf_ch3_xPNRZ = conv(bp,ch3_xPNRZ);

bp = basePulse(mp,Fs,"bipolar");
delay_rec = round(numel(bp)/2);
delay_total = filter_order/2 + delay_rec;
rf_ch1_xBPNRZ = conv(bp,ch1_xBPNRZ);
rf_ch2_xBPNRZ = conv(bp,ch2_xBPNRZ);
rf_ch3_xBPNRZ= conv(bp,ch3_xBPNRZ);

bp = basePulse(mp,Fs,"manchester");
delay_rec = round(numel(bp)/2);
delay_total = filter_order/2 + delay_rec;
rf_ch1_xMCHSTR = conv(bp,ch1_xMCHSTR);
rf_ch2_xMCHSTR = conv(bp,ch2_xMCHSTR);
rf_ch3_xMCHSTR= conv(bp,ch3_xMCHSTR);

figure;
subplot(4,1,1);
showPulseTrain(xUNRZ, mp, 10, filter_order, "unipolar", "off");
subplot(4,1,2);
showPulseTrain(rf_ch1_xUNRZ, mp, 10, filter_order, "unipolar ch1", "on");
subplot(4,1,3);
showPulseTrain(rf_ch2_xUNRZ, mp, 10, filter_order, "unipolar ch2", "on");
subplot(4,1,4);
showPulseTrain(rf_ch2_xUNRZ, mp, 10, filter_order, "unipolar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xPNRZ, mp, 10, filter_order, "polar", "off");
subplot(4,1,2);
showPulseTrain(rf_ch1_xPNRZ, mp, 10, filter_order, "polar ch1", "on");
subplot(4,1,3);
showPulseTrain(rf_ch2_xPNRZ, mp, 10, filter_order, "polar ch2", "on");
subplot(4,1,4);
showPulseTrain(rf_ch2_xPNRZ, mp, 10, filter_order, "polar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xBPNRZ, mp, 10, filter_order, "bipolar", "off");
subplot(4,1,2);
showPulseTrain(rf_ch1_xBPNRZ, mp, 10, filter_order, "bipolar ch1", "on");
subplot(4,1,3);
showPulseTrain(rf_ch2_xBPNRZ, mp, 10, filter_order, "bipolar ch2", "on");
subplot(4,1,4);
showPulseTrain(rf_ch2_xBPNRZ, mp, 10, filter_order, "bipolar ch3", "on");

figure;
subplot(4,1,1);
showPulseTrain(xMCHSTR, mp, 10, filter_order, "manchester", "off");
subplot(4,1,2);
showPulseTrain(rf_ch1_xMCHSTR, mp, 10, filter_order, "manchester ch1", "on");
subplot(4,1,3);
showPulseTrain(rf_ch2_xMCHSTR, mp, 10, filter_order, "manchester ch2", "on");
subplot(4,1,4);
showPulseTrain(rf_ch2_xMCHSTR, mp, 10, filter_order, "manchester ch3", "on");

%% IV. Cuantificador
received_bits_ch1_xUNRZ = quantifier(rf_ch1_xUNRZ, mp, filter_order, Fs);
received_bits_ch2_xUNRZ = quantifier(rf_ch2_xUNRZ, mp, filter_order, Fs);
received_bits_ch3_xUNRZ = quantifier(rf_ch3_xUNRZ, mp, filter_order, Fs);

received_bits_ch1_xPNRZ = quantifier(rf_ch1_xPNRZ, mp, filter_order, Fs);
received_bits_ch2_xPNRZ = quantifier(rf_ch2_xPNRZ, mp, filter_order, Fs);
received_bits_ch3_xPNRZ = quantifier(rf_ch3_xPNRZ, mp, filter_order, Fs);

received_bits_ch1_xBPNRZ = quantifier(rf_ch1_xBPNRZ, mp, filter_order, Fs);
received_bits_ch2_xBPNRZ = quantifier(rf_ch2_xBPNRZ, mp, filter_order, Fs);
received_bits_ch3_xBPNRZ = quantifier(rf_ch3_xBPNRZ, mp, filter_order, Fs);

received_bits_ch1_xMCHSTR = quantifier_m(rf_ch1_xMCHSTR, mp, filter_order, Fs);
received_bits_ch2_xMCHSTR = quantifier_m(rf_ch2_xMCHSTR, mp, filter_order, Fs);
received_bits_ch3_xMCHSTR = quantifier_m(rf_ch3_xMCHSTR, mp, filter_order, Fs);


if(ber_estimate == 1)

fprintf("BER UNRZ ch1 = %.1f %% \r",   error_estimate(bits,received_bits_ch1_xUNRZ));
fprintf("BER UNRZ ch2 = %.1f %%\r",   error_estimate(bits,received_bits_ch2_xUNRZ));
fprintf("BER UNRZ ch3 = %.1f %%\r\n", error_estimate(bits,received_bits_ch3_xUNRZ));

fprintf("BER PNRZ ch1 = %.1f %%\r", error_estimate(bits,received_bits_ch1_xPNRZ));
fprintf("BER PNRZ ch2 = %.1f %%\r", error_estimate(bits,received_bits_ch2_xPNRZ));
fprintf("BER PNRZ ch3 = %.1f %%\r\n", error_estimate(bits,received_bits_ch3_xPNRZ));

fprintf("BER BPNRZ ch1 = %.1f %%\r", error_estimate(bits,received_bits_ch1_xBPNRZ));
fprintf("BER BPNRZ ch2 = %.1f %%\r", error_estimate(bits,received_bits_ch2_xBPNRZ));
fprintf("BER BPNRZ ch3 = %.1f %%\r\n", error_estimate(bits,received_bits_ch3_xBPNRZ));

fprintf("BER MCHSTR ch1 = %.1f %%\r", error_estimate(bits,received_bits_ch1_xMCHSTR));
fprintf("BER MCHSTR ch2 = %.1f %%\r", error_estimate(bits,received_bits_ch2_xMCHSTR));
fprintf("BER MCHSTR ch3 = %.1f %%\r\n", error_estimate(bits,received_bits_ch3_xMCHSTR));

end

figure;
subplot(4,1,1);
quantify_received_data(bits);
title('Original Picture');
subplot(4,1,2);
quantify_received_data(received_bits_ch1_xUNRZ);
title('Unipolar NRZ Channel 1 Picture, fc = 0.3060');
subplot(4,1,3);
quantify_received_data(received_bits_ch2_xUNRZ);
title('Unipolar NRZ Channel 2 Picture, fc = 0.1560');
subplot(4,1,4);
quantify_received_data(received_bits_ch3_xUNRZ);
title('Unipolar NRZ Channel 3 Picture, fc = 0.0600');

figure;
subplot(4,1,1);
quantify_received_data(bits);
title('Original Picture');
subplot(4,1,2);
quantify_received_data(received_bits_ch1_xPNRZ);
title('polar NRZ Channel 1 Picture, fc = 0.3060');
subplot(4,1,3);
quantify_received_data(received_bits_ch2_xPNRZ);
title('polar NRZ Channel 2 Picture, fc = 0.1560');
subplot(4,1,4);
quantify_received_data(received_bits_ch3_xPNRZ);
title('polar NRZ Channel 3 Picture, fc = 0.0600');

figure;
subplot(4,1,1);
quantify_received_data(bits);
title('Original Picture');
subplot(4,1,2);
quantify_received_data(received_bits_ch1_xBPNRZ);
title('Bipolar NRZ Channel 1 Picture, fc = 0.3060');
subplot(4,1,3);
quantify_received_data(received_bits_ch2_xBPNRZ);
title('Bipolar NRZ Channel 2 Picture, fc = 0.1560');
subplot(4,1,4);
quantify_received_data(received_bits_ch3_xBPNRZ);
title('Bipolar NRZ Channel 3 Picture, fc = 0.0600');

figure;
subplot(4,1,1);
quantify_received_data(bits);
title('Original Picture');
subplot(4,1,2);
quantify_received_data(received_bits_ch1_xMCHSTR);
title('manchester NRZ Channel 1 Picture, fc = 0.3060');
subplot(4,1,3);
quantify_received_data(received_bits_ch2_xMCHSTR);
title('manchester Channel 2 Picture, fc = 0.1560');
subplot(4,1,4);
quantify_received_data(received_bits_ch3_xMCHSTR);
title('manchesterChannel 3 Picture, fc = 0.0600');


function bits = lena2bits()
    snip_size = 32;
    im_width = 350;
    im_height = im_width - ((snip_size*2)+1);

    % cargar imagen de lena
    load 'lena512.mat';
    % recortar imagen
    lenarec=lena512((im_height-snip_size):im_height,(im_width-snip_size):im_width);
    % convertir imagen a binario
    b=de2bi(lenarec,8,'left-msb');
    % calcular la transpuesta del vector
    b=b';
    % obtener vector de bits concatenado
    bits=b(:);
    newBits = reshape(bits,[8,1089]);
    % % calcular la transpuesta del vector de bits (1089 x 8)
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamaño del vector (33 x 33)
     newLena = reshape(newLena,[33,33]);
    % mostrar imagen 
    imshow(uint8(newLena));
end

function UNRZ = unipolarNRZ(mp, bits, Fs, pot, display_st)
    rs = Fs/mp;
    unrz = 1*ones(1,mp);
    % normalizacion de la potencia a 1 W
    unrz = unrz/sqrt(energy_calc(unrz,Fs));
    unrz = unrz*sqrt(1/rs);

    if(display_st == "on")
        fprintf("displaying pulse base in wvtool\n\r");
        wvtool(pulso_base);
    else
        fprintf("pulse base display disabled\n\r");
    end

    % generar tren de impulsos (1)
    s = zeros(1,numel(bits)*mp);
    % generar tren de impulsos (2)
    s(1:mp:end) = bits;    
    % generar tren de pulsos
    UNRZ = conv(unrz,s);
end

function PNRZ = polarNRZ(mp, bits, Fs, pot, display_st)
    s1 = bits;
    rs = Fs/mp;
    % analisis de potencia de la senal por pulso
    pnrz = 1*ones(1,mp);
    pnrz = pnrz/sqrt(energy_calc(pnrz,Fs));
    pnrz = pnrz*sqrt(1/rs);

    %pnrz = pnrz/sqrt(energy_calc(pnrz,Fs));
    %pnrz = pnrz*sqrt(0.5)
    s1(s1==0) = -1; 
    s = zeros(1,numel(s1)*mp);
    s(1:mp:end) = s1;
    PNRZ = conv(pnrz,s);
end

function BPNRZ = bipolarNRZ(mp, bits, Fs, pot, display_st)
    s1 = bits;
    rs = Fs/mp;
    % analisis de potencia de la senal por pulso
    bpnrz = 1*ones(1,mp);
    
    last_i = 1;
    for i = 1:size(s1,1)
        if(s1(i) == 1)
            % Alternar entre 1 y -1
            last_i  = last_i*-1;
            s1(i) = last_i;
        end
    end
    
    s = zeros(1,numel(s1)*mp);
    s(1:mp:end) = s1;
    BPNRZ = conv(bpnrz,s);
    BPNRZ = BPNRZ/sqrt(power_calc(BPNRZ));
end

function MCHSTR = manchester(mp, bits, Fs, pot, display_st)
    pm = [ones(1,mp/2),-1*ones(1,mp/2)];

    if(display_st == "on")
        fprintf("displaying pulse base in wvtool\n\r");
        wvtool(pm);
    else
        fprintf("pulse base display disabled\n\r");
    end

    s1 = bits;
    s1(s1==0) = -1;
    s = zeros(1,(numel(bits)-1)*mp+1);
    s(1:mp:end) = s1;
    MCHSTR = conv(pm,s);
end

function lpf = channelFilter(filter_order, mp, fc, fv_disp)
    fprintf("fc = %.4f\n\r", fc);
    fprintf("filter order = %.1f\n\r", filter_order);
    % vector de frecuencia del filtro pasabajas con fc = 0.8 rad/muestra 
    f = [0 fc fc 1];
    % vector de magnitud del del filtro pasabajas con fc = 0.8 rad/muestra
    m = [1 1 0 0];
    lpf = fir2(filter_order, f, m);
    if(fv_disp == "show")
        fprintf("Displaying fvtool graph\n\r");
        fvtool(lpf);
    else 
        fprintf("fvtool graph not shown\n\r");
    end
end 

function filt_pt = setCommunicationChannel(xPulse,filter)
    filt_pt = conv(xPulse,filter);
end

function showPulseTrain(xPulse, mp, n_pulses, filter_order, graph_title, channel_st)
    if(channel_st == "on")
        stem(xPulse((filter_order/2):mp*n_pulses+filter_order/2),'r')
        title(strcat(graph_title," pulse train"));;
        xlabel('samples(n)');
        ylabel('impulse d(n)');
        xlim([0,100])
    else
        stem(xPulse(1:mp*n_pulses),'r');
        title(strcat(graph_title," pulse train"));
        xlabel('samples(n)');
        ylabel('impulse d(n)');
    end
end

function pulseTrainPSD(xPulse,Fs)
    % graficar densidad espectral de potencia 
    pwelch(xPulse, [], [], [], Fs,'power');  
end

%{
    Quantifier for UNRZ,PNRZ,BPNRZ line encoding
%}
function receivedBits = quantifier(xPulse, mp, filter_order, Fs)
    global bits_size
    xPulse(xPulse > 1) = 1;
    xPulse(xPulse < 0) = 0;

    test = xPulse(filter_order/2+mp/2:mp:end-(filter_order/2));
    roundedTest = round(test);
    roundedTest(roundedTest < 0) = roundedTest(roundedTest<0)+1;
    receivedBits = roundedTest';
    receivedBits = receivedBits(1:bits_size);
end


%{
    Quantifier for the manchester line encoding
%}
function receivedBits = quantifier_m(xPulse, mp, filter_order, Fs)
    global bits_size
    xPulse(xPulse > 0) = 1;
    xPulse(xPulse < 0) = 0;

    test = xPulse(filter_order/2+mp/2:mp:end-(filter_order/2));
    roundedTest = round(test);
    roundedTest(roundedTest < 0) = roundedTest(roundedTest<0)+1;
    receivedBits = roundedTest';
    receivedBits = receivedBits(1:bits_size);
end

function quantify_received_data(receivedBits)
    %{
    % reasignar el tamano del vector (8 x 1089)
    newBits = reshape(receivedPulse,[8,1089]);
    % calcular la transpuesta del vector de bits (1089 x 8)
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamano del vector (33 x 33)
    newLena = reshape(newLena,[33,33]);
    %} 
    newBits = reshape(receivedBits,[8,1089]);
    newBits = newBits';
    % convertir imagen a decimal para poder ser representada
    newLena = bi2de(newBits,'left-msb');
    % reasignar el tamano del vector (33 x 33)
    newLena = reshape(newLena,[33,33]);
    % mostrar imagen 
    imshow(uint8(newLena));
end

function pow = power_calc(xPulse)
    pow = (1/numel(xPulse))*sum(xPulse*xPulse');
end

function energy = energy_calc(xPulse,Fs)
    energy = (1/Fs)*sum(xPulse);
end

function ber = error_estimate(bits_transmitted,bits_received)
    global bits_size
    ber = biterr(bits_transmitted,bits_received)/bits_size;
    ber = ber*100;
end


function bp = basePulse(mp,Fs,filter_type)
    rs = Fs/mp;
    if(filter_type == "unipolar")
        unrz = 1*ones(1,mp);
        % normalizacion de la potencia a 1 W
        unrz = unrz/sqrt(energy_calc(unrz,Fs));
        unrz = unrz*sqrt(1/rs);
        bp = unrz;
    elseif(filter_type == "polar")
        pnrz = 1*ones(1,mp);
        pnrz = pnrz/sqrt(energy_calc(pnrz,Fs));
        pnrz = pnrz*sqrt(1/rs);
        bp = pnrz;
    elseif(filter_type == "bipolar")
        bpnrz = 1*ones(1,mp);
        bp = bpnrz;
    elseif(filter_type == "manchester")
        pm = [ones(1,mp/2),-1*ones(1,mp/2)];
        bp = pm;
    end
end

