clear; clc; close all;
pulso = triang(100); % pulso triangular de 100 muestras

%{
  Triangular
%}
Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100) % elemento 100 de la convolucion

figure;
subplot(2,1,1);
stem(pulso);
title('triangular pulse');
xlabel('samples');
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of triangular pulse');
xlabel('samples');

fprintf("energy Triangular = %.2f\r", Ep);
fprintf("autocorrelation max Triangular = %.2f\r", E1);
fprintf("autocorrelation 100th element Triangular = %.2f\r\n", E2(100));

%{
   Half sine
%}
x = 0 : pi/100 : pi;
pulso = sin(x);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy halfsine = %.2f\r", Ep);
fprintf("autocorrelation max halfsine = %.2f\r", E1);
fprintf("autocorrelation 100th element halfsine = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('halfsine pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of halfsine pulse');
xlabel('samples');
xlim([0,200]);

%{
  Rectangular
%}
pulso = rectwin(100);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy rectangular = %.2f\r", Ep);
fprintf("autocorrelation max rectangular = %.2f\r", E1);
fprintf("autocorrelation 100th element rectangular = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('rectangular pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of halfsine pulse');
xlabel('samples');
xlim([0,200]);

%{
  Gausswin
%}
pulso = gausswin(100);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy Gausswin = %.2f\r", Ep);
fprintf("autocorrelation max Gausswin = %.2f\r", E1);
fprintf("autocorrelation 100th element Gausswin = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('Gausswin pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of Gausswin pulse');
xlabel('samples');
xlim([0,200]);


%{
  Chebwin
%}
pulso = chebwin(100);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy Chebwin = %.2f\r", Ep);
fprintf("autocorrelation max Chebwin = %.2f\r", E1);
fprintf("autocorrelation 100th element Chebwin = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('Chebwin pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of Chebwin pulse');
xlabel('samples');
xlim([0,200]);

%{
  Kaiser
%}
pulso = kaiser(100);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy Kaiser = %.2f\r", Ep);
fprintf("autocorrelation max Kaiser = %.2f\r", E1);
fprintf("autocorrelation 100th element Kaiser = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('Kaiser pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of Kaiser pulse');
xlabel('samples');
xlim([0,200]);


%{
  Tukeywin
%}
pulso = tukeywin(100);

Ep = sum(pulso.*pulso); % energia del pulso
E1 = max(conv(pulso,fliplr(pulso))); % maximo de la convolucion
E2 = conv(pulso,fliplr(pulso));
E2(100); % elemento 100 de la convolucion

fprintf("energy Tukeywin = %.2f\r", Ep);
fprintf("autocorrelation max Tukeywin = %.2f\r", E1);
fprintf("autocorrelation 100th element Tukeywin = %.2f\r\n", E2(100));

figure;
subplot(2,1,1);
stem(pulso);
title('Tukeywin pulse');
xlabel('samples');
xlim([0,100]);
subplot(2,1,2);
stem(conv(pulso,fliplr(pulso)))
title('autocorrelation of Tukeywin pulse');
xlabel('samples');
xlim([0,200]);