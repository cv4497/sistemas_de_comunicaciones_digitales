function pulso_base = pulsoBase(mp,display_st)
    pulso_base = rectwin(mp);

    if(display_st == "on")
        fprintf("displaying pulse base in wvtool\n\r");
        wvtool(pulso_base);
    else(display_st == "off")
        fprintf("pulse base display disabled\n\r");
    end
end
    
function UNRZ = unipolarNRZ(mp,bits)
    pulso_base = pulsoBase(mp,"on");
    % generar tren de impulsos (1)
    s = zeros(1,numel(bits)*mp);
    % generar tren de impulsos (2)
    s(1:mp:end) = bits;    
    % generar tren de pulsos
    UNRZ = conv(pulso_base,s);
end