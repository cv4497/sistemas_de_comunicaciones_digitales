clear; clc; close all;
%% ejericicio 1
% cargar imagen de lena
load 'lena512.mat';
% recortar imagen
lenarec=lena512(252:284,318:350);
% convertir imagen a binario
b=de2bi(lenarec,8,'left-msb');
% calcular la transpuesta del vector
b=b';
% obtener vector de bits concatenado
bits=b(:);
% reasignar el tama�o del vector (8 x 1089)
newBits = reshape(bits,[8,1089]);
% calcular la transpuesta del vector de bits (1089 x 8)
newBits = newBits';
% convertir imagen a decimal para poder ser representada
newLena = bi2de(newBits,'left-msb');
% reasignar el tamano del vector (33 x 33)
newLena = reshape(newLena,[33,33]);
% mostrar imagen 
imshow(uint8(newLena));
%% ejercicio 2
% secuencia aleatoria de bits
bits = randi([0 1], 65536,1);
% muestras por pulso 
mp = 10;                     
% frecuencia de muestreo
Fs = 96000;
% periodo de muestreo
Ts = 1/Fs;                   
% generar un solo pulso
R = Fs/mp;

% Unipolar NRZ
% analisis de potencia de la senal por pulso
pnrz = 1*ones(1,mp);% wvtool(pnrz);                 
% generar tren de impulsos (1)
s = zeros(1,numel(bits)*mp);  
% generar tren de impulsos (2)
s(1:mp:end) = bits;           
% generar tren de pulsos
xUNRZ = conv(pnrz,s);
% crear figura
figure;                               
subplot(2,1,1);
% graficar tren de pulsos
stem(s(1:mp*16),'r');            
title('Unipolar NRZ train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(2,1,2);
% graficar densidad espectral de potencia
pwelch(xUNRZ, [], [], [], Fs,'power'); 

% Polar NRZ
s1 = bits;
% analisis de potencia de la se�al por pulso
pnrz = 1*ones(1,mp);% wvtool(pnrz); 
% Convertir �0� en �-1�
s1(s1==0) = -1; 
s = zeros(1,numel(s1)*mp);
s(1:mp:end) = s1;
xPNRZ = conv(pnrz,s);
% crear figura
figure;                                
subplot(3,1,1);
plot(xPNRZ(1:mp*16),'r')
title('polar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(3,1,2);
stem(xPNRZ(1:mp*16),'r')
title('polar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(3,1,3);
pwelch(xPNRZ,500,300,500,Fs,'power');

% Bipolar NRZ
s1 = bits;
% analisis de potencia de la se�al por pulso
pnrz = 1*ones(1,mp);% wvtool(pnrz); 

last_i = 1;
for i = 1:size(s1,1)
    if(s1(i) == 1)
        % Alternar entre 1 y -1
        last_i  = last_i*-1;
        s1(i) = last_i;
    end
end

s = zeros(1,numel(s1)*mp);
s(1:mp:end) = s1;
xPNRZ = conv(pnrz,s);
figure;                                
subplot(3,1,1);
plot(xPNRZ(1:mp*16),'r')
title('bipolar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(3,1,2);
stem(xPNRZ(1:mp*16),'r')
title('bipolar NRZ pulse train');
xlabel('samples(n)');
ylabel('impulse d(n)');
subplot(3,1,3);
pwelch(xPNRZ,500,300,500,Fs,'power');
