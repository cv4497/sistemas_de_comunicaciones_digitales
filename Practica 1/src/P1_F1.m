    %{  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
%{
     Autor: Cesar Villarreal Hernandez
     Titulo: Practica I
     Descripcion: Fase I
     Asignatura: Sistemas de Comunicaciones Digitales
     Fecha: 11/noviembre/2020
%}
%{ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %}
close all; clc; clear;

psd_pt = 0;

fs = 96000;
mp = 16;
t = 0:1/fs:5;
f = 5000;
a = 1;
% senal senoidal para calibracion del canal
y1 = a*sin(2*pi*f*t);
%soundsc(y1,fs);

%% Senal CHIRP
fi = 50;
ff = 20e3;
y = chirp(t,fi,5,ff,'linear');
plot(y);
%soundsc(y,fs);

impulse = [zeros(1,fs) 1 zeros(1,fs)];
%soundsc(impulse,fs);

%% Senal Ruido Gaussiano

close all; clc;
% frecuencia de muestreo
Fs = 96000;
% periodo de muestreo
ts = 1/fs;
%tiempo final
tf = 5;
% tiempo inicial
ti = 0;
% vector de tiempo
t = ti:ts:tf-ti;
% generacion de ruido gaussiano
n = sqrt(1)*randn(1,length(t));
% reproduccion del ruido al canal
%soundsc(n,fs);

%% muestreo y espectro de potencia de la senal capturada

impulse_file = 'IMPULSE_RESPONSE.wav';
chirp_file = 'CHIRP.wav';
gauss_file = 'GAUSSIAN_NOISE.wav';

[x_impulse, Fs] = audioread(impulse_file);
x_impulse = x_impulse(:,1);

%{ calculo del vector de tiempo %}
TotalTime = length(x_impulse)./Fs;                    % duracion del audio
dt = (TotalTime/(length(x_impulse)));                 % paso de tiempo
tf = (TotalTime-(TotalTime/length(x_impulse)));       % tiempo final
t = 0:dt:tf;                                  % definicion del v. tiempo

% grafica de la senal muestreada
figure;
stem(t,x_impulse,'r'); 
title('Grafica de la senal del impulso');
% espectro de potencia de la senal muestreada
if(psd_pt)
figure;
pwelch(x_impulse, [0 10], [], [], Fs,'power'); 
title('Espectro de la senal del impulso');
end

[x_chirp, Fs] = audioread(chirp_file);
x_chirp = x_chirp(:,1);

%{ calculo del vector de tiempo %}
TotalTime = length(x_chirp)./Fs;                    % duracion del audio
dt = (TotalTime/(length(x_chirp)));                 % paso de tiempo
tf = (TotalTime-(TotalTime/length(x_chirp)));       % tiempo final
t = 0:dt:tf;                                  % definicion del v. tiempo

% grafica de la senal muestreada
figure;
plot(t,x_chirp,'r'); 
title('Grafica de la senal chirp');
% espectro de potencia de la senal muestreada
if(psd_pt)
figure;
pwelch(x_chirp, [0 10], [], [], Fs,'power'); 
title('Espectro de la senal chirp');
end

[x_gauss, Fs] = audioread(gauss_file);
x_gauss = x_gauss(:,1);

%{ calculo del vector de tiempo %}
TotalTime = length(x_gauss)./Fs;                    % duracion del audio
dt = (TotalTime/(length(x_gauss)));                 % paso de tiempo
tf = (TotalTime-(TotalTime/length(x_gauss)));       % tiempo final
t = 0:dt:tf;       

% grafica de la senal muestreada
figure;
plot(t,x_gauss,'r'); 
title('Grafica de la senal chirp');
% espectro de potencia de la senal muestreada
if(psd_pt)
figure;
pwelch(x_gauss, [0 10], [], [], Fs,'power'); 
title('Espectro de la senal chirp');
end

function [xq,t,Fs, dt, tf, TotalTime] = sample_audiofile(file_name,k)
    [X, Fs] = audioread(file_name);
    X = X(:,1);
    
    % calculo del vector de tiempo
    TotalTime = length(X)./Fs;                    % duracinn del audio
    dt = (TotalTime/(length(X)));                 % paso de tiempo
    tf = (TotalTime-(TotalTime/length(X)));       % tiempo final
    t = 0:dt:tf;                                  % definicinn del v. tiempo
    
    % senal muestreada con sample-and-hold
    ts = 2;
    xs = zeros(1,length(t));
    
    for i=1:length(t)
    if(rem(i,ts)==1)
    tmp = X(i);
    end
    xs(i) = tmp;
    end

    % cuantificacinn
    M = 2^k;
    fprintf("k = %i\n", k);

    int = (max(xs)-min(xs))/M;
    m = (min(xs)+int/2):int:(max(xs)-int/2);
    xq = zeros(1,length(t));
    
    for i=1:length(t)
        [tmp k] = min(abs(xs(i)-m));
        xq(i) = m(k);
    end
end


function spectrogram_audio_vector(X, Fs, title_graph)
    figure;
    spectrogram(X,2000,0,5000,Fs,'yaxis');        % grnfica del espectrograma
    colormap jet
    xlim([0 10]);                                  % limites del eje x
    ylim([0 15]);                                  % limites del eje y
    xticks(0:0.5:10);                              % paso del eje x
    title('Espectrograma');                       % tntulo de la grnfica
    xlabel('Tiempo (segundos)');                  % nombre del eje x
    ylabel('Frecuencia (kHz)');                    % nombre del eje y
end
