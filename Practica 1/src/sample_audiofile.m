function [xq,t,Fs, dt, tf, TotalTime] = sample_audiofile(file_name,k)
    [X, Fs] = audioread(file_name);
    X = X(:,1);
    
    % calculo del vector de tiempo
    TotalTime = length(X)./Fs;                    % duracion del audio
    dt = (TotalTime/(length(X)));                 % paso de tiempo
    tf = (TotalTime-(TotalTime/length(X)));       % tiempo final
    t = 0:dt:tf;                                  % definicion del v. tiempo
    
    % senal muestreada con sample-and-hold
    ts = 2;
    xs = zeros(1,length(t));
    
    for i=1:length(t)
    if(rem(i,ts)==1)
    tmp = X(i);
    end
    xs(i) = tmp;
    end

    % cuantificacion
    M = 2^k;
    fprintf("k = %i\n", k);

    int = (max(xs)-min(xs))/M;
    m = (min(xs)+int/2):int:(max(xs)-int/2);
    xq = zeros(1,length(t));
    
    for i=1:length(t)
        [tmp k] = min(abs(xs(i)-m));
        xq(i) = m(k);
    end
end